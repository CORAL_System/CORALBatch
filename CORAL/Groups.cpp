

#include "stdafx.h"
#include "Groups.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <cmath>
#include <map>
#include <set>
#include <algorithm>
#include <numeric>
#include <limits>
#include "Muzammil.h"


#define INF 1e20       //Pseudo Infitinte number for this code
int totalIterations=0;

GroupOperation::~GroupOperation()
{

}
GroupOperation::GroupOperation(double STinput, int Ndata, int Ldata)
{
    //myGroup->seqCentroidDist[1]
    groupCounter=0;
    ST=STinput;
    N=Ndata;
    L=Ldata;

    globalDist=INF;




}
double distFuncG(double x, double y)
{

        return (pow((x - y), 2));

}

//reads the time series data from input file and store in structure
//n is number of time series and L is length of time series, skip is true if first number of each time series should be skipped
int GroupOperation::readFile(const char * Filename, int number, int length)
{
    inputFile.open(Filename); //Opening the file
    N=number;
    L=length;
    min=10000;
    max=0;
    //double skipNum;
    if(!inputFile)
    {
        cout << "there's something wrong with the file!\n"; //Throw exception
        return -1;
    }
    else
    {
        //read the input file into time series structure
        //Read numbers from file into the array
        for(int i = 0; i < N; i++)
        {
            for(int j=0; j<L;j++)
            {
                inputFile>>timeSeries[i][j];
                if(timeSeries[i][j]<min)
                    min=timeSeries[i][j];
                if(timeSeries[i][j]>max)
                    max=timeSeries[i][j];
            }
        }
        inputFile.close();

        return 1;
    }



}
 //prints the time series
void GroupOperation::printTS()
{
    //cout<<"Printing Timeseries"<<endl;
    for(int i=0;i<N;i++)
    {
        for(int j=0;j<L;j++)
        {
            cout<<timeSeries[i][j]<<" ";
        }
        cout<<endl;
    }

}
//prints all the times and asociated information (group ids, count of groups)
void GroupOperation::printTime()
{

    timeFile.open(timeFilePath.c_str());
    //cout<<endl<<endl;
    for (int l=1; l<L; l++)
    {

   //     cout<<"Time intervals: "<<l<<" "<<m<<endl;
        timeFile<<"T "<<l<<" Groups "<<Time[l].groupid.size()<<endl;
        int groupCount=Time[l].groupid.size();
        for(int k=0;k<groupCount;k++)
        {
            timeFile<<"G"<<" "<<Time[l].groupid[k]<<" ";

        }
        timeFile<<endl;
     }
    timeFile.close();
}
//prints alll groups with associated information
void GroupOperation::printGroups()
{


    groupFile.open(GroupFilePath.c_str());
    cout<<"Total Groups "<<groupArray.size()<<endl;
    groupFile<<"Total"<<" "<<groupArray.size()<<endl;
    groupFile<<"Min "<<min<<" "<<"Max "<<max<<endl;
    int TScount=groupArray.size();
    for(int i=0;i<TScount;i++)
    {
 //       cout<<"Group "<<groupArray[i].id<<endl;
        groupFile<<"G "<<groupArray[i].id<<" "<<groupArray[i].count<<endl;

        for(int k=0;k<groupArray[i].seqObj.size();k++)
        {
            groupFile<<groupArray[i].seqObj[k].id<<" "<<groupArray[i].seqObj[k].startT<<" "<<groupArray[i].seqObj[k].endT<<" ";
        }
        groupFile<<endl;

        groupFile<<"CentroidLen "<<groupArray[i].centroid.size()<<" "<<groupArray[i].centroidID<<" "<<groupArray[i].centroidStart<<endl;
        for(int k =0;k<groupArray[i].centroid.size();k++)
        {
            groupFile<<groupArray[i].centroid[k]<<" ";
        }
        groupFile<<endl;
    }

    groupFile.close();

}
//picks multiple groups in each iteration.
//CORAL method for grouping, length is grouping sequences for the length specified, delta determines hwo much variation from sqrt (N) seq group can be picked
void GroupOperation::qualityGroupingMultiple(int length, double delta)
{
    //first get the initial set of centroid
    for(int i=0;i<N;i++)
    {
        for(int j=0;j<L-length;j++)
        {
            sequencesGrouped[i][j]=false;
            centroidFlag[i][j]=false;
        }
    }
    //first get the initial set of centroid
    int centroidCount=0;
    int iterationCount=0;
    normFactor=/*sqrt*/(length);
    vector<subsequence> tempSeqObj;

    double seqMean=0.0;
    double seqStd=0.0;

    double EDist=0;
    int seqLeft=N*(L-length);

    int centPick=(int)sqrt(seqLeft);
    int seqTotal=centPick;       //these many need to be in group

 //   cout<<"Total Sequences to be Grouped "<<seqLeft;
    //writing in group File
    /*
    groupResults<<seqLeft;
    groupResults<<',';
    groupResults<<centPick;
    groupResults<<',';
*/
    vector<double> tempSZN; //temp z-nromalized time series

    while(seqLeft>0)
    {
        iterationCount++;
        totalIterations++;
        
        for(int i=0;i<L-length;i++)
        {
            //Check each subsequence
            for(int j=0;j<N;j++)
            {
                //check if this sequence is not yet grouped
                if(sequencesGrouped[j][i]==false)
                {
                    if(centroidCount<=centPick)   //if sqrt N centroids are picked exit from here
                    {
                        //make first centroid
                        if(centroidCount==0)
                        {
                            Group *newGroup=new Group();
                            subsequence *newSeq=new subsequence;
                            newGroup->count=1;
                            //z-normalize this centroid
                            for(int k=i;k<=i+length;k++)
                                newGroup->centroid.push_back(timeSeries[j][k]);
                            znormSequence(newGroup->centroid, seqMean,seqStd);

                            newSeq->id=j;
                            newSeq->startT=i;
                            newGroup->centroidID=j;
                            newGroup->centroidStart=i;
                            newGroup->mean=seqMean; //setting mean of centroid
                            newGroup->std=seqStd;   //setting std for centroid
                            newSeq->mean=seqMean;
                            newSeq->std=seqStd;
                            newSeq->endT=length+newSeq->startT;
                            centroidFlag[j][i]=true;

                            newGroup->seqObj.push_back(*newSeq);
                            tempCentroids.push_back(*newGroup);
                            delete newSeq;
                            centroidCount++;
                            delete newGroup;
                        }
                        else    //check with other centroids
                        {
                            int currentCentroids=centroidCount;
                            bool distFlag=false;
                            EDist=0;
                            tempSZN.clear();
                            for(int l=0;l<=length;l++)
                            {
                                tempSZN.push_back(timeSeries[j][i+l]);
                            }
                            znormSequence(tempSZN, seqMean,seqStd);
                            for(int k=0;k<currentCentroids;k++)
                            {
                                //timeseries needs to be z-normalized
                                for(int l=0;l<=length;l++)
                                    EDist+=(tempSZN[l]-tempCentroids[k].centroid[l])*(tempSZN[l]-tempCentroids[k].centroid[l]) ;
                                 EDist=sqrt(EDist);
                              //   tempSZN.clear();
                                if(EDist<(ST*(normFactor)))   //it is within ST of any centroid
                                {
                                    distFlag=true;
                                //    tempSZN.clear();
                                    break;

                                }
                                EDist=0;

                            }
                            if(distFlag==false)
                            {
                                //make this a new centroid
                                //make new group
                                Group *nGroup=new Group();
                                nGroup->id=0;//groupCounter++;
                                nGroup->count=1;
                                subsequence *newSeq=new subsequence;
                                newSeq->id=j;
                                newSeq->startT=i;
                                newSeq->endT=i+length;
                                newSeq->mean=seqMean;
                                newSeq->std=seqStd;
                                nGroup->centroidID=j;
                                nGroup->mean=seqMean;
                                nGroup->std=seqStd;
                                nGroup->centroidStart=i;
                                centroidFlag[j][i]=true;
                                //here centroid needs to be z-normalized
                                for(int k=0;k<=length;k++)
                                    nGroup->centroid.push_back(tempSZN[k]);
                              //  tempSZN.clear();
                                nGroup->seqObj.push_back(*newSeq);
                               // nGroup->seqObj.push_back(*newSeq);
                                tempCentroids.push_back(*nGroup);
                                delete newSeq;
                                centroidCount++;
                                delete nGroup;
                            }


                        }
                    }
                    else
                    {
                        //sequences are still left to be picked
                        if(centroidCount==0)
                        {
                            Group *newGroup=new Group();
                            subsequence *newSeq=new subsequence;
                            newGroup->count=1;
                            //here centroid needs to be z-normalized
                            for(int k=i;k<=i+length;k++)
                                newGroup->centroid.push_back(timeSeries[j][k]);
                            znormSequence(newGroup->centroid, seqMean,seqStd);
                            newGroup->mean=seqMean; //setting mean of centroid
                            newGroup->std=seqStd;   //setting std for centroid
                            newSeq->mean=seqMean;
                            newSeq->std=seqStd;
                            newSeq->id=j;
                            newSeq->startT=i;
                            newGroup->centroidID=j;
                            newGroup->centroidStart=i;
                            newSeq->endT=length+newSeq->startT;
                            centroidFlag[j][i]=true;

                            newGroup->seqObj.push_back(*newSeq);
                            tempCentroids.push_back(*newGroup);
                            delete newSeq;
                            centroidCount++;
                            delete newGroup;
                        }
                        else    //check with other centroids
                        {
                            int currentCentroids=centroidCount;
                            bool distFlag=false;
                            EDist=0;
                            tempSZN.clear();
                            for(int l=0;l<=length;l++)
                            {
                                tempSZN.push_back(timeSeries[j][i+l]);
                            }
                            znormSequence(tempSZN, seqMean,seqStd);
                            for(int k=0;k<currentCentroids;k++)
                            {
                                for(int l=0;l<=length;l++)
                                    EDist+=(tempSZN[l]-tempCentroids[k].centroid[l])*(tempSZN[l]-tempCentroids[k].centroid[l]) ;
                                 EDist=sqrt(EDist);
                                if(EDist<(ST*(normFactor)))   //it is within ST of any centroid
                                {
                                    distFlag=true;
                                  //  tempSZN.clear();
                                    break;

                                }
                                EDist=0;

                            }
                            if(distFlag==false)
                            {
                                //make this a new centroid
                                //make new group
                                Group *nGroup=new Group();
                                nGroup->id=0;//groupCounter++;
                                nGroup->count=1;
                                subsequence *newSeq=new subsequence;
                                newSeq->id=j;
                                newSeq->startT=i;
                                newSeq->endT=i+length;
                                nGroup->centroidID=j;
                                nGroup->centroidStart=i;
                                centroidFlag[j][i]=true;
                                //here centroid needs to be z-normalized
                                for(int k=i;k<=i+length;k++)
                                    nGroup->centroid.push_back(timeSeries[j][k]);
                                znormSequence(nGroup->centroid, seqMean,seqStd);
                                nGroup->mean=seqMean; //setting mean of centroid
                                nGroup->std=seqStd;   //setting std for centroid
                                newSeq->mean=seqMean;
                                newSeq->std=seqStd;
                                nGroup->seqObj.push_back(*newSeq);
                               // nGroup->seqObj.push_back(*newSeq);
                                tempCentroids.push_back(*nGroup);
                            //    tempSZN.clear();
                                delete newSeq;
                                centroidCount++;
                                delete nGroup;
                            }


                        }

                    }

                }

                }

            }

    //at this point initial temp centroids have been picked
    //assign each subsequence to the cluster
    //    bool groupFound=false;
        for(int i=0;i<L-length;i++)
        {
            //Check each subsequence
            for(int j=0;j<N;j++)
            {
                for(int l=0;l<=length;l++)
                {
                    tempSZN.push_back(timeSeries[j][i+l]);
                }
                znormSequence(tempSZN, seqMean,seqStd);
                for(int k=0;k<centroidCount;k++)
                {
                    if(sequencesGrouped[j][i]==false)
                    {
                        if(centroidFlag[j][i]==true)
                            break;       //this sequence is already a centroid
                        else
                        {
                            EDist=0;
                            //here timeseries needs to be z-normalized

                            for(int l=0;l<=length;l++)
                                EDist+=(tempSZN[l]-tempCentroids[k].centroid[l])*(tempSZN[l]-tempCentroids[k].centroid[l]) ;
                             EDist=sqrt(EDist);
                            if(EDist<(ST*normFactor)/2)   //it is within ST/2 of centroid and it is not same as centroid
                            {
                                subsequence *newSeq=new subsequence;
                                newSeq->id=j;
                                newSeq->startT=i;
                                newSeq->endT=i+length;
                                newSeq->mean=seqMean;
                                newSeq->std=seqStd;
                                //update the bit vector
                                tempCentroids[k].seqObj.push_back(*newSeq);
                                delete newSeq;
                                tempCentroids[k].count++;



                            }

                        }

                    }
                }
                tempSZN.clear();

            }
        }
        //at this point sequences have been placed in groups
        //at this point pick all the groups having closest to sqrt N sequences and put in final group
        int bestGroupIndex=0;
        int bestGroupDev=INF;

        bool groupFlag=false;
        for(int i=0;i<centroidCount;i++)
        {
            //this should be cent pick
            int seqCount=abs(tempCentroids[i].count-seqTotal);
            //try to avoid picking groups with only 1 member
            if(tempCentroids[i].count==1 )        //no other group has been picked
            {
                if(bestGroupDev==INF)   //no other group has been picked
                {
                    if(bestGroupDev>seqCount)
                    {
                        bestGroupDev=seqCount;
                        bestGroupIndex=i;
                    }
                }
            }
            else
            {
                if(bestGroupDev>seqCount)
                {
                    bestGroupDev=seqCount;
                    bestGroupIndex=i;
                }
            }
            /*
            if(bestGroupDev>seqCount)
            {
                bestGroupDev=seqCount;
                bestGroupIndex=i;
            }
            */
            if(seqCount<=ceil((delta/100)*seqTotal))
            {
                groupFlag=true;
                //this groups should be added
                //first iterate over all its sequences to remove any overlapping sequences
                //iterate over al the sequences in this group and replace from original set
                int tempCount=tempCentroids[i].count;
                tempSeqObj.clear();

                for(int j=0;j<tempCount;j++)
                {

                    int TSindex=tempCentroids[i].seqObj[j].id;
                    int start=tempCentroids[i].seqObj[j].startT;

                    if(sequencesGrouped[TSindex][start]==false)
                    {

                        sequencesGrouped[TSindex][start]=true;
                        seqLeft--;
                        subsequence *tempNewSeq=new subsequence;
                        tempNewSeq->id=TSindex;
                        tempNewSeq->startT=start;
                        tempNewSeq->endT=tempCentroids[i].seqObj[j].endT;
                        tempNewSeq->mean=tempCentroids[i].seqObj[j].mean;
                        tempNewSeq->std=tempCentroids[i].seqObj[j].std;
                        tempSeqObj.push_back(*tempNewSeq);
                        delete tempNewSeq;
                    }


                }
                //add this group to final groups list
                //should not add if 0 sequences are there
                if(tempSeqObj.size()!=0)
                {
                    tempCentroids[i].seqObj.clear();    //erase all
                    tempCentroids[i].seqObj=tempSeqObj;
                    tempCentroids[i].count=tempSeqObj.size();
                    tempCentroids[i].id=groupCounter++;
                 //   tempCentroids[i].centroidMean=std::accumulate(tempCentroids[i].centroid.begin(), tempCentroids[i].centroid.end(), 0.0)/tempCentroids[i].centroid.size();

                    centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart]=true;
                //    cout<<"Seq inside group "<<tempCentroids[i].count<<endl;
                    groupArray.push_back(tempCentroids[i]);
                 //   groupResults<<tempCentroids[i].count;
                 //   groupResults<<',';

                    //one more centroid has ben picked
                    centPick--;
               //     cout<<" , "<<tempCentroids[i].count;
                    //add this group to the the list of groups for this time interval
                    Time[length].groupid.push_back(tempCentroids[i].id);
                }

            }
            else
            {
                //this centroid is not picked
                 centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart]=false;
            }


        }
        if(groupFlag==false)
        {
            //none of the group has delta sqrt N sequences, promote the closest one
            tempCentroids[bestGroupIndex].id=groupCounter++;
            centroidFlag[tempCentroids[bestGroupIndex].centroidID][tempCentroids[bestGroupIndex].centroidStart]=true;
         //   tempCentroids[bestGroupIndex].centroidMean=std::accumulate(tempCentroids[bestGroupIndex].centroid.begin(), tempCentroids[bestGroupIndex].centroid.end(), 0.0)/tempCentroids[bestGroupIndex].centroid.size();
         //   cout<<"Seq inside group "<<tempCentroids[bestGroupIndex].count<<endl;
            groupArray.push_back(tempCentroids[bestGroupIndex]);
           // groupResults<<tempCentroids[bestGroupIndex].count;
          //  groupResults<<',';
            centPick--;
        //    cout<<" , "<<tempCentroids[bestGroupIndex].count;
            //add this group to the the list of groups for this time interval
            Time[length].groupid.push_back(tempCentroids[bestGroupIndex].id);
            int tempCount=tempCentroids[bestGroupIndex].count;
            for(int j=0;j<tempCount;j++)
            {

                int TSindex=tempCentroids[bestGroupIndex].seqObj[j].id;
                int start=tempCentroids[bestGroupIndex].seqObj[j].startT;

                if(sequencesGrouped[TSindex][start]==false)
                {

                    sequencesGrouped[TSindex][start]=true;
                    seqLeft--;

                }

            }
        }
        tempCentroids.clear();
        centroidCount=0;

     }
}



//groups the time series
int GroupOperation::groupOp(int method, double delta=10.0)
{
    groupArray.clear();
    totalIterations=0;
    //intilize again the groupCounter
    groupCounter=0;
  //  groupResults<<"Length"<<','<<"TotalSeq"<<','<<"Exp"<<','<<"Group Seq"<<','<<"Groups"<<endl;
    //now group the time series for all intervals

	SequenceIndexMap.resize(L - 1);
	for(int j=1;j<L;j++)
    {
     //   cout<<"Length "<<j<<endl;

        Time[j].groupid.clear();
     //   groupResults<<j<<',';
        if(method==1)
            qualityGroupingMultiple(j,delta);
        else if(method==2)
            groupNewOp(j);
        cout<<" Groups generated at length "<<j<<" -> "<<Time[j].groupid.size()<<endl;
     //   groupResults<<Time[j].groupid.size()<<',';
     //   groupResults<<endl;

	std::vector<std::vector<bool>>	Map = GetSequenceIndexSlices(j);
	SequenceIndexMap.at(j-1) = Map;

	MakeInterRepCorrMap(j);

    }

	PrintGroupCitizenship(5, 3);

	std::vector<int> CommonGroupVec;
	int SsLength = -1;
	int output = GetLongestMatchingSsGroups(8, 11, 15, CommonGroupVec, SsLength);

    //printing all the groups
  //  groupResults.close();

//    cout<<"Total Iterations"<<totalIterations<<endl;
    cout<<"Total Groups "<<groupArray.size()<<endl;
    return groupArray.size();


}
//Greedy grouping method, changed so that centroid doesn't change
void GroupOperation::groupNewOp(int length)
{
    //Time[length].count=0;
    Group *newGroup;//=new Group();
    subsequence *newSeq=new subsequence;
    double EDDist=0;
    int bestGroupIndex=0;
    double bsf=INF;       //best so far distance
    int startTime=0;
    bestGroupIndex=0;
    vector<double> tempSeries;
    vector<double> tempSZN; //temp z-nromalized time series
    double seqMean=0.0;
    double seqStd=0.0;
    normFactor=/*sqrt*/(length);    //normalized euclidean factor
    //itarte over time intervals of length given
    for(int i=0;i<L-length;i++)
    {
        //pick subsequences
        for(int j=0;j<N;j++)
        {
            if(Time[length].groupid.size()==0)  //no groups created yet
            {
                newGroup=new Group();
                //make the first group

                newGroup->id=groupCounter++;
                //add this group to the the list of groups for this time interval
                Time[length].groupid.push_back(newGroup->id);
                newGroup->count=1;

                for(int i=startTime;i<=length;i++)
                {
                    //first new centroid is first time series
                    newGroup->centroid.push_back(timeSeries[0][i]);
                  //  newGroup->sum.push_back(timeSeries[0][i]);
                }
                //znormalize centroid
                znormSequence(newGroup->centroid, seqMean,seqStd);

                newGroup->mean=seqMean; //setting mean of centroid
                newGroup->std=seqStd;   //setting std for centroid
                newSeq->mean=seqMean;
                newSeq->std=seqStd;
                newSeq->id=j;
                newSeq->startT=i;
                newSeq->endT=length;

                newGroup->seqObj.push_back(*newSeq);
                newGroup->centroidID=j;
                newGroup->centroidStart=i;
           //     newGroup->centroidMean=std::accumulate( newGroup->centroid.begin(), newGroup->centroid.end(), 0.0)/newGroup->centroid.size();
                delete newSeq;
                groupArray.push_back(*newGroup);
                delete newGroup;

            }
            else
            {
                //check with the groups
                bsf=INF;
                int currentGroups=Time[length].groupid.size();  //get the number of groups for this length
                tempSZN.clear();
                for(int l=0;l<=length;l++)
                {
                    tempSZN.push_back(timeSeries[j][i+l]);
                }
                znormSequence(tempSZN, seqMean,seqStd);
                for(int k=0;k<currentGroups;k++)
                {
                    EDDist=0;
                    int groupID=Time[length].groupid[k];

                    for(int l=0;l<=length;l++)
                    {
                        //compare time series with centroid one at a time
                        EDDist+=(tempSZN[l]-groupArray[groupID].centroid[l])*(tempSZN[l]-groupArray[groupID].centroid[l]) ;
                    }

                        EDDist=sqrt(EDDist);

                    if(EDDist<bsf)
                    {
                        bsf=EDDist;
                        bestGroupIndex=groupID;
                    }

                }
                if(bsf<=(ST*normFactor)/2)
                {
                    groupArray[bestGroupIndex].count++; //increase the count of TS in this group

                    //add this subsequence in the group
                    subsequence *newSeq=new subsequence;
                    newSeq->id=j;
                    newSeq->startT=i;
                    newSeq->endT=i+length;
                    newSeq->mean=seqMean;
                    newSeq->std=seqStd;

                    //update the bit vector
                    groupArray[bestGroupIndex].seqObj.push_back(*newSeq);
                    delete newSeq;
                }
                else
                {
                    //make new group
                    Group *nGroup=new Group();
                    nGroup->id=groupCounter++;
                    subsequence *newSeq=new subsequence;
                    newSeq->id=j;
                    newSeq->startT=i;
                    newSeq->endT=i+length;
                    newSeq->mean=seqMean;
                    newSeq->std=seqStd;
                    nGroup->mean=seqMean;
                    nGroup->std=seqStd;

                    nGroup->seqObj.push_back(*newSeq);
                    nGroup->centroidID=j;
                    nGroup->centroidStart=i;

                    //add this group id to this time interval groups
                    Time[length].groupid.push_back(nGroup->id);

                    nGroup->count=1;

                    //this is the centroid of this group

                    for(int k=0;k<=length;k++)
                    {
                        //first new centroid is first time series
                        nGroup->centroid.push_back(tempSZN[k]);
                      //  nGroup->sum.push_back(timeSeries[j][k]);
                    }

                    tempSZN.clear();
                    //push this group in groups vector
               //     nGroup->centroidMean=std::accumulate( nGroup->centroid.begin(), nGroup->centroid.end(), 0.0)/nGroup->centroid.size();
                    groupArray.push_back(*nGroup);
                    delete newSeq;
                    delete nGroup;
                }
            }
        }
    }
}
void GroupOperation::znormSequence(vector<double> &t1, double &mean, double &std)
{
    double gMean=0;   //mean of the sequences
    double gStd=0;     //std of sequences
    double sum=0;


  //  cout<<"Z-normalize sequence"<<endl;
    //here in z-normalize also compute the max in the z-normalize sequences and in end divide each with that max
    double max=0;   //initializing with first element

    gStd=0;
    sum=0;
    gMean=0;
    //calculate sum of sequence
    for(int j=0;j<t1.size();j++)
    {
        sum+=t1[j];
    }
    //calculating mean of this sequence
    gMean=sum/t1.size();
    //calculating std of these sequences
    for(int j=0;j<t1.size();j++)
    {

        gStd+=pow(t1[j]-gMean,2);
    }
    gStd=sqrt((gStd/t1.size()));
    //z-normalize this sequence

	if (gStd == 0)
	{
		gStd = std::numeric_limits<double>::epsilon();
	}

    //max=fabs((max-gMean)/gStd);
    for(int j=0;j<t1.size();j++)
    {
        t1[j]=(t1[j]-gMean)/gStd;
        if(fabs(t1[j])>max)
            max=fabs(t1[j]);    //update the max
        //t1[j]=t1[j]/max;

    }
   //now divide each term in z-normalized sequence with the max value so that its range is from [-1,1]

	if (max < 1)
		max=max+1;

    for(int j=0;j<t1.size();j++)
    {
        t1[j]=t1[j]/max;
    }
    mean=gMean;
    std=gStd;
}

// ONLINE OPERATIONS
void GroupOperation::znormSequenceS(vector<double> &t1)
{
    double gMean=0;   //mean of the sequences
    double gStd=0;     //std of sequences
    double sum=0;


    gStd=0;
    sum=0;
    gMean=0;
    //calculate sum of sequence
    for(int j=0;j<t1.size();j++)
    {
        sum+=t1[j];
    }
    //calculating mean of this sequence
    gMean=sum/t1.size();
    //calculating std of these sequences
    for(int j=0;j<t1.size();j++)
    {
        gStd+=pow(t1[j]-gMean,2);
    }
    gStd=sqrt((gStd/t1.size()));
    //z-normalize this sequence

    for(int j=0;j<t1.size();j++)
    {
        t1[j]=(t1[j]-gMean)/gStd;

    }

}

//reads the query file
//type =true means normalize and false means running for Spring don't normalize
void GroupOperation::readQueryFile(int m,const char * Filename, bool type)
{
    tempQ.clear();
    // Array for keeping the query data
    Q = (double *)malloc(sizeof(double)*m);
    queryLength=m;
    queryFile.open(Filename);
    //read the query data and normalize at the same time
    for(int i=0;i<m;i++)
    {
        queryFile>>Q[i];
        if(type==true)
        {
            Q[i]=(Q[i]-min)/(max-min);
            queryN<<Q[i];
            queryN<<" ";
        }
        tempQ.push_back(Q[i]);
    }
        queryN.close();
    queryFile.close();
}

//finds k similar time series, query Flag=true query sequence is given
//timeFlag=true time interval Exact, false means Any



void GroupOperation::loadQueryTS(int TSid, int startTS, int endTS)
{
    //load subsequence if TS in query object
    tempQ.clear();
    //also write this query in file as well
    queryF.open("C:/Qt/Tools/QtCreator/bin/build-CORAL-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/queryInside.txt");
    for(int i=startTS;i<=endTS;i++)
    {
        tempQ.push_back(timeSeries[TSid][i]);
        queryF<<timeSeries[TSid][i];
        if(i!=endTS)
            queryF<<" ";
      //  cout<<timeSeries[TSid][i]<<" ";
    }
    queryF.close();
}
//correlation function for naive method, it z-normalizes the sequences first and then compute with same formula of dot product
double GroupOperation::correlation(vector<double>& t1, vector<double>& t2)
{
    //z-normalize the two sequences
    znormSequenceS(t1);
    znormSequenceS(t2);
    /*
    double meanX=0;
    double meanY=0;
    double stdX=0;
    double stdY=0;
    */
    double corr=0;
    /*
    meanX= std::accumulate( t1.begin(), t1.end(), 0.0)/t1.size();
    meanY=std::accumulate( t2.begin(), t2.end(), 0.0)/t2.size();
    //calculate standard deviations
    for(int i=0;i<t1.size();i++)
        stdX+=pow(t1[i]-meanX,2);
    stdX=sqrt(stdX/t1.size());
    for(int j=0;j<t2.size();j++)
        stdY+=pow(t2[j]-meanY,2);
    stdY=sqrt(stdY/t2.size());
*/
    //calculate correlation measure

    for(int i =0;i<t1.size();i++)
    {
  //      double term1=(t1[i]-meanX)/stdX;
    //    double term2=(t2[i]-meanY)/stdY;
        double term=t1[i]*t2[i];
        corr=corr+ term;
    }
    corr=corr/t1.size();
    return corr;
}

//correlation computation function for CORAL
//just a dot product between the two values, sequences passed are already z-normalized
double GroupOperation::CORALcorrelation(const vector<double>& t1, const vector<double>& t2)
{
    double corr=0;

    //calculate correlation measure

    for(int i =0;i<t1.size();i++)
    {
        double term=t1[i]*t2[i];
        corr=corr+ term;
    }
    corr=corr/t1.size();
    return corr;
}

//naive method of finding the best match, compares with every subsequence
void GroupOperation::springCalculation()
{
    double bsf=0;
   // double actualCorr;
    int bsfIndex;       //index of bsf time series
    double currentCorr=INF;
    int bestIntervalS;
    int bestIntervalE;
    vector<double> temp;    //temporary subsequence
    //calculate mean of query sequence
//    double meanQ=std::accumulate(tempQ.begin(),tempQ.end(),0.0);
    //get each subsequence
    for(int j=1;j<L;j++)
    {
        for (int l=0, m=l+j; m<L; l++, m++)
        {
             for (int i=0;i<N;i++)
             {

                 //copy  the subsequence in temp
                 for(int p=0;p<=m-l;p++)
                     temp.push_back(timeSeries[i][l+p]);


                 //check if method is any point wise

                 if(temp.size()==tempQ.size())
                 {
                     currentCorr=correlation(temp,tempQ);

                     if(fabs(currentCorr)>fabs(bsf))
                     {
                         bsf=currentCorr;
                         bsfIndex=i;        //best time series index
                         bestIntervalS=l;   //record the best interval
                         bestIntervalE=m;
                     }
                     //clear the temp array
                     temp.clear();
                     currentCorr=0;
                 }
                 else
                     temp.clear();

             }
         }
     }

    //print the closest sequence
    cout<<"Closest sequence <";
    cout<<"TS "<<bsfIndex<<" Interval "<<bestIntervalS<<" "<<bestIntervalE<<" Corr "<<bsf<<endl;
    temp.clear();
}

//reads time information from the file containing the count of groups in each time interval
void GroupOperation::readTime(const char * Filename)
{
	/*
    inputTimeFile.open(Filename);
    //read till the end of file
    string temp;
    int length;
    int count;
    while(inputTimeFile)
    {

        inputTimeFile>>temp;    //read T
        inputTimeFile>>length;  //read length interval
        inputTimeFile>>temp;        //read Groups
        inputTimeFile>>count;  //count of groups in this interval

        for(int i=0;i<count;i++)
        {
            inputTimeFile>>temp;    //read G
            int id;
            inputTimeFile>>id;
            Time[length].groupid.push_back(id);
        }
        if(length==L-1)
            break;
    }
    inputFile.close();
	*/
}
//reads the groups attributes from a file and saves in group structure
void GroupOperation::readGroups(const char * Filename)
{
	/*
    groupInFile.open(Filename);
    string temp;
    //int startT;
   // int endT;
    int count;
    int totalcount;
    subsequence *newSeq;
    Group *newGroup;//=new Group();
    groupInFile>>temp;        //reading total
    groupInFile>>totalcount;
    groupInFile>>temp;        //reading min
    groupInFile>>min;
    groupInFile>>temp;        //reading max
    groupInFile>>max;
    while(groupInFile)
    {
        newGroup=new Group();
        groupInFile>>temp;        //reading G
        groupInFile>>newGroup->id;    //group id
        groupInFile>>newGroup->count; //count of TS
        //read Sequences in this group
        for(int i=0;i<newGroup->count;i++)
        {
            newSeq=new subsequence;
            groupInFile>>newSeq->id;
            groupInFile>>newSeq->startT;
            groupInFile>>newSeq->endT;
            newGroup->seqObj.push_back(*newSeq);
            delete newSeq;
        }
        //read centroid
        groupInFile>>temp;        //reading Centroid
        groupInFile>>count;   //centroid length
        groupInFile>>newGroup->centroidID;
        groupInFile>>newGroup->centroidStart;
    //    groupFile>>newGroup->centroidMean;
        //newGroup->centroidLen=count;
        for(int i=0;i<count;i++)
        {
            double value;
            groupInFile>>value;
            newGroup->centroid.push_back(value);
        }
        groupArray.push_back(*newGroup);
        if(newGroup->id==totalcount-1)
        {
            //all groups read
            delete newGroup;
            groupInFile.close();
            return;
        }
        delete newGroup;

    }
    groupInFile.close();
	*/
}
//based on new quality grouping find similar with specific length
double GroupOperation::kSimilarnew(int length)
{
    double bsf=0;

    int bsfIndex;       //index of bsf centroid
    int bsfTSIndex;
    double dist=0;
    double currentDist=0;
    int numGroups=Time[length-1].groupid.size();       //get the number of groups for this length
    //z-normalize the query seq

 //   double meanQ=std::accumulate(tempQ.begin(),tempQ.end(),0.0);
    for(int i=0;i<numGroups;i++)
    {
        //check the closest group
        int groupID=Time[length-1].groupid[i];
            //check if the group is of same length as query


        //compute point wise corrrelation distance
        //centroids are alread z-normalized so no need to z-normalize again
        currentDist=correlation(groupArray[groupID].centroid, tempQ);
        if(((int)fabs(currentDist)*100)/100==1.0)    //perfect correlation found, stop
        {
            cout<<groupArray[groupID].centroidID<<"["<<groupArray[groupID].centroidStart<<","<<groupArray[groupID].centroidStart+length-1<<"] "<<"corr "<<currentDist<<endl;
            return currentDist;     //found the centroid with corr=1 , return
        }
        if(fabs(currentDist)>fabs(bsf))
        {
            bsf=currentDist;
            bsfIndex=groupID;
        }

    }

 //at this point have the best group
    //compute with each sequence present in that group
    bsf=0;
    vector<double> tempSeries;
    bsfTSIndex=0;
    currentDist=0;
    int bestStart=0;
    int bestEnd=0;
    dist=0;
    for(int i=0;i<groupArray[bsfIndex].seqObj.size();i++)
    {
        int seqID=groupArray[bsfIndex].seqObj[i].id;
        int seqStart=groupArray[bsfIndex].seqObj[i].startT;
        int seqEnd=groupArray[bsfIndex].seqObj[i].endT;
        //copy this time series in tempSeries
        //z-normalize while copying the data
        for(int j=seqStart;j<=seqEnd;j++)
        {
            tempSeries.push_back((timeSeries[seqID][j]- groupArray[bsfIndex].seqObj[i].mean)/groupArray[bsfIndex].seqObj[i].std);
        }

       //compute point wise correlation distance
        dist=correlation(tempSeries,tempQ);
        if(((int)fabs(dist)*100)/100==1.0)       //found perfect correlation ,stop
        {
            cout<<seqID<<"["<<seqStart<<","<<seqEnd<<"] "<<"corr "<<dist<<endl;
            return dist;

        }
        if(fabs(bsf)<fabs(dist))
        {
            bsf=dist;
            bsfTSIndex=seqID;
            bestStart=seqStart;
            bestEnd=seqEnd;
        }
        tempSeries.clear();
        dist=0;


    }
    cout<<bsfTSIndex<<"["<<bestStart<<","<<bestEnd<<"] "<<" Corr "<<bsf<<endl;

    return bsf;


}
void GroupOperation::corrSequences(int seq1, int start1, int end1,int seq2, int start2, int end2)
{
    vector<double> temp1;
    vector<double> temp2;
    //double value=0;
    for(int i=start1;i<=end1;i++)
    {
        temp1.push_back(timeSeries[seq1][i]);
    }
    for(int i=start2;i<=end2;i++)
    {
        temp2.push_back(timeSeries[seq2][i]);
    }

    cout<<"Correlation "<<correlation(temp1,temp2)<<endl;
    temp1.clear();
    temp2.clear();
}
//outputs the self correlating sequences of time series TS passed that have correlation above
//groupVector contains the group ids TS subsequences belong to
void GroupOperation::selfCorrelation(const vector<int> groupVector, int TS)
{

    int count=0;    //count of sequences in each group, output just count
    for(int i=0;i<groupVector.size();i++)
    {
        //iterate over each group
        int groupID=groupVector[i];
        Group *nGroup=new Group();
        *nGroup=groupArray[groupID];
        for(int j=0;j<nGroup->seqObj.size();j++)
        {
            if(nGroup->seqObj[j].id==TS)
            {
                cout<<TS<<"["<<nGroup->seqObj[j].startT<<"]"<<" ";
                count++;
            }
        }
        cout<<"Total Seq in group"<<count<<endl;
        count=0;


    }
}
