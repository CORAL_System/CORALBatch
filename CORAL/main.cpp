/***********************************************************************/
/************************* DISCLAIMER **********************************/
/***********************************************************************/
/**
/**                                                                   **/
/** Unless stated otherwise, all software is provided free of charge. **/
/** As well, all software is provided on an "as is" basis without     **/
/** warranty of any kind, express or implied. Under no circumstances  **/
/** and under no legal theory, whether in tort, contract,or otherwise,**/
/** shall be liable to you or to any other **/
/** person for any indirect, special, incidental, or consequential    **/
/** damages of any character including, without limitation, damages   **/
/** for loss of goodwill, work stoppage, computer failure or          **/
/** malfunction, or for any and all other damages or losses.          **/
/**                                                                   **/
/** If you do not agree with these terms, then you you are advised to **/
/** not use this software.                                            **/
/***********************************************************************/
/***********************************************************************/
//VERSION OF SYSTEM TO IMPLEMENT GDTW DISTANCES


#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include "Groups.h"
#include <string>
#include <time.h>
#include <iostream>
//#include "online.h"
//#include <sys/time.h>
#include <math.h>
#include <time.h>


using namespace std;
void groupScale(const char * Filename, const char * outFilename);
/*
void groupScale(const char * Filename, const char * outFilename)
{

    ifstream testDatasets;
    ofstream outputFile;
    testDatasets.open(Filename);
    outputFile.open(outFilename);
    string filePaths="C:/Qt/Tools/QtCreator/bin/build-ONEXComparitive-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/NormalizeBatchDatasets/";
    string outPath="C:/Qt/Tools/QtCreator/bin/build-CORAL-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/CSVMulti/";
    string filename;
    int timeseriesN;
    int timeseriesL;
    string dataset;
    int numGroups;
  //  int method=1;
    double ST=0.2;
    int numFiles=0;
    testDatasets>>numFiles;
    while(numFiles>0) //read all datasets one by one
    {
        testDatasets>>filename;
        dataset=filePaths+filename+".txt";
        testDatasets>>timeseriesN;      //reading number of time series
        testDatasets>>timeseriesL;      //reading length of time series
        GroupOperation *groupObj=new GroupOperation(ST,timeseriesN,timeseriesL);     //ST passed as input
        groupObj->readFile(dataset.c_str(), timeseriesN,timeseriesL);
        clock_t start = clock();
        int choice=1;
        string outputFileC=outPath+filename+".csv";
        cout<<"Dataset "<<filename<<" "<<timeseriesN<<" "<<timeseriesL<<endl;
        groupObj->groupResults.open(outputFileC.c_str());
        numGroups= groupObj->groupOp(choice,10.0);
        groupObj->groupResults.close();
        outputFile<<filename<<" ";
        outputFile<<numGroups<<" ";
        outputFile<<(clock()-start)/double(CLOCKS_PER_SEC) << " [s]";

        outputFile<<endl;
        numFiles--;
    }
    outputFile.close();
}
/*
void preprocess(const char * Filename, int N, int L, double ST, string filePath)
{


        GroupOperation *groupObj=new GroupOperation(ST);     //ST passed as input
        groupObj->readFile(Filename, N,L);
        //groupObj->normalize();


        groupObj->timeFilePath=filePath;
        groupObj->timeFilePath.append("Time.txt");
        groupObj->GroupFilePath=filePath.append("Groups.txt");
        groupObj->groupResults.open("C:/Qt/Tools/QtCreator/bin/build-CORAL-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/groupResults.csv");
        cout<<"Enter 0 for single group, 1 for Multiple groups, 2 for Onex grouping, 3 for Random Centroids, 4 for for MaxSeq"<<endl;
        int choice;
        cin>>choice;
        double delta=10.0;
        if(choice!=2)
        {
            cout<<"Enter delta"<<endl;
            cin>>delta;
        }
      //  groupObj->groupResults.close();
        clock_t start = clock();
        groupObj->groupOp(choice,delta);
        cout << "Offline Construction " << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl;
        //print groups and length index in file
        groupObj->printGroups();
        groupObj->printTime();

}
*/
int main()
{
    int arg1,arg2,arg3, arg5,arg6,arg7;
    double arg4;
    string filename;
 //   string filenameMatrix;
    string queryFilename;
    cout<<"Load data file, Enter filename followed by N L"<<endl;
    cin>>filename;
    cin>>arg2;//N
    cin>>arg3;//L
    int queryLength;
    string filePath;

	/*
	MUZ DEBUG
	*/
	//groupScale("InputFileName.txt", "OutputFileName.txt");

	/*
	MUZ DEBUG
	*/

    //read data
    GroupOperation *groupObj=new GroupOperation(0.2,arg2,arg3);// ST, number of time series in original file, length of time series
    groupObj->readFile(filename.c_str(),arg2,arg3);
    cout<<"Time series read"<<endl;

  //  onlineObj->gdtw();

    cout<<"Enter 0 to preprocess the data, 1 to find correlation through brute force, 2 through Coral, 3 to find correlation bw 2 sequences, 4 to exit"<<endl;
    cin>>arg1;

    while(arg1!=4)
    {
        if(arg1==0)
        {
            cout<<"Enter ST"<<endl;
            cin>>arg4;//ST
        //    cout<<"Enter path for writing groups and time files"<<endl;
        //    cin>>filePath;

            //groupObj->timeFilePath=filePath;
           // groupObj->timeFilePath.append("Time.txt");
          //  groupObj->GroupFilePath=filePath.append("Groups.txt");
       //     groupObj->groupResults.open("C:/Qt/Tools/QtCreator/bin/build-CORAL-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/groupResults.csv");
            cout<<"Enter 1 for Multiple groups, 2 for Greedy grouping"<<endl;
            int choice;
            cin>>choice;
            double delta=10.0;
            if(choice!=2)
            {
                cout<<"Enter delta"<<endl;
                cin>>delta;
            }
          //  groupObj->groupResults.close();
            clock_t start = clock();
            groupObj->groupOp(choice,delta);
            cout << "Offline Construction " << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl;
            //print groups and length index in file
           // groupObj->printGroups();
            //groupObj->printTime();



/*
            cout<<"Enter path for reading groups and time file"<<endl;
            cin>>filePath;
            onlineObj->timeFilePath=filePath;
           onlineObj->timeFilePath.append("Time.txt");
            onlineObj->readTime(onlineObj->timeFilePath.c_str());
            onlineObj->GroupFilePath=filePath.append("Groups.txt");
            //read groups genereted in offline step
            onlineObj->readGroups(onlineObj->GroupFilePath.c_str());
            */

        }
        else if(arg1==1)
        {
            cout<<" BRUTE FORCE"<<endl;

            //cout<<"preprocess filename"<<endl;
            cout<<"Enter 1 to load query from time series, 2 from query file"<<endl;
            cin>>arg2;
            if(arg2==1) //load from time series
            {
                cout<<"<queryTS, start, end"<<endl;
                cin>>arg2;
                cin>>arg3;
                cin>>arg5;
               groupObj->loadQueryTS(arg2,arg3,arg5);
            }
            else
            {
                cout<<"<queryFile, query length"<<endl;
                cin>>queryFilename;
                cin>>queryLength;
                groupObj->readQueryFile(queryLength,queryFilename.c_str(),false);
            }


        clock_t start1;
            start1 = clock();
          // cout<<"Spring Calculation"<<endl;
            groupObj->springCalculation();//onlineObj->tempQ.size()-1);
           cout << "Naive " << (clock()-start1)/double(CLOCKS_PER_SEC) << "s]" << endl;


        }
        else if(arg1==2)
        {

            cout<<"Enter 1 to load query from time series, 2 from query file"<<endl;
            cin>>arg2;
            if(arg2==1) //load from time series
            {
                cout<<"<queryTS, start, end"<<endl;
                cin>>arg2;
                cin>>arg3;
                cin>>arg5;
               groupObj->loadQueryTS(arg2,arg3,arg5);
            }
            else
            {
                cout<<"<queryFile, query length"<<endl;
                cin>>queryFilename;
                cin>>queryLength;
                groupObj->readQueryFile(queryLength,queryFilename.c_str(),false);
            }


            int queryLength=groupObj->tempQ.size();
            //z-normalize query sequence
            groupObj->znormSequenceS(groupObj->tempQ);
            clock_t start1 = clock();
            //search only same length
            groupObj->kSimilarnew(queryLength);

          //  cout<<"Best distance "<<dist<<endl;
            cout << "Coral " << (clock()-start1)/double(CLOCKS_PER_SEC) << "s]" << endl;
            //cout<<"Best TS "<<onlineObj->kbest.id<<" ["<<onlineObj->kbest.startT<<" "<<onlineObj->kbest.endT<<"]"<<endl;

        }
        /*
        else if(arg1==3)    //group for multiple datasets
        {
            cout<<"Enter file name with full path"<<endl;
            string inputFilePath;
            string outputFilePath;
            cin>>inputFilePath;
            cout<<"Enter output file path"<<endl;
            cin>>outputFilePath;
            groupScale(inputFilePath.c_str(),outputFilePath.c_str());


        }
        */
        else if(arg1==3)    //find correlation between 2 sequences
        {
            cout<<"seq1 start end seq2 start end method"<<endl;
            cin>>arg2;//seq1
            cin>>arg3;//start
            cin>>arg4;//end
            cin>>arg5;//seq2
            cin>>arg6;//start
            cin>>arg7;//end
            groupObj->corrSequences(arg2,arg3,arg4,arg5,arg6,arg7);
        }

        cout<<"Enter 0 to preprocess the data, 1 to find correlation through brute force, 2 through Coral,3 to find correlation bw 2 sequences,4 to exit"<<endl;
        cin>>arg1;
    }


}


