#include "stdafx.h"
//#include "Muzammil.h"
#include "Groups.h"
#include <stdio.h>


void TestCall(void)
{
	printf("This is test call\n");

}

std::vector<std::vector<bool>> GroupOperation::GetSequenceIndexSlices(int j)
{

	std::vector<int> CurrentGroupArrayIndex = Time[j].groupid;
	int nCurrentGroups = CurrentGroupArrayIndex.size();

	std::vector<bool> GroupFlag(nCurrentGroups, false);
	std::vector<std::vector<bool>> Map(N, GroupFlag);


	/*!
	Group array for this length j is looped over and each sequence is checked for its TimeSeries ID.

	*/
	for (int k = 0; k < nCurrentGroups; k++)
	{
		int ThisGroupIndex = CurrentGroupArrayIndex[k]; // index of group under consideration
		Group *hThisGroup = &groupArray[ThisGroupIndex]; // group handle for group under consideration

		for (int l = 0; l < hThisGroup->seqObj.size(); l++) // loop for all the sequences of this group
		{
			int i = hThisGroup->seqObj.at(l).id;			// ID (time series) of each sub-sequence in group is extracted

			Map.at(i).at(k) = true;
		}
	}
	return Map;
}

std::vector<int> GroupOperation::PrintGroupCitizenship(int i, int j)
{
	std::vector<bool> GroupFlagVec = SequenceIndexMap[i][j];
	int GroupIdOffset = Time[i].groupid[0];
	std::vector<int> GroupCitizenshipVec;

	// Loop over the GroupFlagVec to see which groups have a SS of length i belonging to time series j
	for (int k = 0; k < GroupFlagVec.size(); k++)
	{
		if (GroupFlagVec[k] == true)
		{
			GroupCitizenshipVec.push_back(k + GroupIdOffset);  // convert to global group ID and push back in a vector
			printf("%d,", k + GroupIdOffset);
		}
	}
	printf("\b \n");		// erase the last comma and give linefeed

	return GroupCitizenshipVec;
}

int GroupOperation::GetLongestMatchingSsGroups(int TimeSeries1, int TimeSeries2, int MinLength, std::vector<int> &CommonGroupVec,int &SsLength)
{
	bool ReturnFlag = false;
	// decrease the length from max to MinLength to see if given time series have common groups
	for (int j = L-1; j >= MinLength; j--)
	{
		// since we store information for any length=j at j-1 location, therefore we extract from it. Other
		// containers have the information for that length at index=j
		std::vector<bool> TS1GroupFlagVec = SequenceIndexMap[j-1][TimeSeries1];
		std::vector<bool> TS2GroupFlagVec = SequenceIndexMap[j-1][TimeSeries2];
		int nGroups = Time[j].groupid.size();
		int GroupOffset = Time[j].groupid[0];
		

		for (int k = 0; k < nGroups; k++)
		{
			if (TS1GroupFlagVec[k] & TS2GroupFlagVec[k])
			{
				CommonGroupVec.push_back(GroupOffset+k);
				ReturnFlag = true;					// since there is atleast one common group, we can return with 0
			}
		}
		
		// return as soon as your find some common group for any i>=MinLength
		if (ReturnFlag == 1)
		{
			SsLength = j;
			return 0;
		}
	}

	SsLength = 0;			// Make subsequence length equal to 0 if no common groups are found
	// incase there is no common group for any i in [L-1,MinLength], then return with -1
	return -1;
}


__int64 GroupOperation::MakeKey(int Key1, int Key2)
{
	// Larger key is shifted to upper 32 bits and added to smaller key to make the unique output key
	// In case both are equal, key2 value is pushed to upper 32 bytes. Observe: Both upper and lower 32bits will have same values
	__int64 Output = 0;

	if (Key1 > Key2)
	{
		Output = (static_cast<__int64>(Key1) << 32) + (static_cast<__int64>(Key2));
	}
	else
	{
		Output = (static_cast<__int64>(Key2) << 32) + (static_cast<__int64>(Key1));
	};

	return Output;
}


void GroupOperation::MakeInterRepCorrMap(int Length)
{
	// For a particular length, correlation of each combination of group representatives is computed and stored in InterRepCorrMap map



	std::vector<int> CurrentGroupArrayID = Time[Length].groupid;
	int StartIdx = CurrentGroupArrayID[0];
	int EndIdx = CurrentGroupArrayID[CurrentGroupArrayID.size() - 1];
	double CorrelationOutput = 0;
	__int64 Key = 0;

	for (int i = StartIdx; i<EndIdx; i++) // i goes from StartIdx to one less than EndIdx
	{
		for (int j = i + 1; j <= EndIdx; j++) // j goes from one more than i to EndIdx
		{
			CorrelationOutput = CORALcorrelation(groupArray[i].centroid, groupArray[j].centroid); 			// Perform coorelation

			Key = MakeKey(i, j); 			// Compute key for (i,j)

			Time[Length].InterRepCorrMap[Key] = CorrelationOutput; 	// insert in map

		}

	}

}