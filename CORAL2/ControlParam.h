#pragma once

#define CORAL_ONLINE_S1_CHECKMULTICLUSTER 0		// Indicates if more than bsf cluster need to be checked 
#define CORAL_ONLINE_S1_CHECKNCLUSTERONLY 1			// Indicates if top N clusters need to be checked for best match correlation or all those falling with in threshold
#define CORAL_ONLINE_S1_CLUSTERS2VISIT 5	// no. of clusters to visit for best match correlation
#define CORAL_ONLINE_S1_PRINTRESULTS 0			// controls wether results should be printed or not