#ifndef GROUPS_H
#define GROUPS_H

#endif // GROUPS_H
#include "structure.h"


// const int timeSeriesN=180;      //Small dataset
// const int timeSeriesL=97;

//const int timeSeriesN=45;      // GUN Point
//const int timeSeriesL=151;

//const int timeSeriesN = 343;      // MedicalImages
//const int timeSeriesL = 100;

//const int timeSeriesN = 94;      // Plane
//const int timeSeriesL = 145;

//const int timeSeriesN = 504;      // FaceAll
//const int timeSeriesL = 132;


//const int timeSeriesN = 1800;      // NonInvasiveFatalECG_Thorax2
//const int timeSeriesL = 751;

const int timeSeriesN = 8033;      // Max N,L
const int timeSeriesL = 2710;

//distance of centroid with the centroids is the same group

class GroupOperation
{
public:
    GroupOperation(double STinput, int Ndata, int Ldata);
    ~GroupOperation();
    int readFile(const char *Filename, int N, int length);    //filename, number of time series and length of each time series

    void printTS();     //prints the time series
    void printTime(); //prints all the times and asociated information (group ids, count of groups)
    void printGroups(); //prints alll groups with associated information

    //offline group Operations
    //int groupOp(int method, double delta);     //groups the time series 
	int groupOp(int method, double delta, int length=0); // length param has been added to run it for single length also 
    //double distED(int TSStartIndex, int TSEndIndex, double centroidStartValue, double centroidEndValue, int m); //calculate ED between TS and centroid
    //void normalize();               //normalize the time series by min and max
    void groupNewOp(int length);      //new grouping methodology

    void qualityGroupingMultiple(int length, double delta); //picks multiple groups
    void znormSequence(vector<double> &t1, double &mean, double &std);   //z-normalize sequence
	void qualityGroupingMultipleOpti(int length, double delta);//optimized version of qualityGroupingMultiple
    //online operations

	void printTSGroups(int TS, int length);
    void readTime(const char *Filename);
    void readGroups(const char *Filename);
    void readQueryFile(int m, const char *Filename, bool type);
    void springCalculation();
    void loadQueryTS(int TSid, int startTS, int endTS);
    double correlation(vector<double> &t1, vector<double> &t2);
    void znormSequenceS(vector<double> &t1);     //z-normalize the sequence without saving mean and std
    double kSimilarnew(int length);         //find the CORAL best match
    double CORALcorrelation(const vector<double>& t1, const vector<double>& t2);
    void corrSequences(int seq1, int start1, int end1,int seq2, int start2, int end2);  //computes the correlation between 2 sequences
    void selfCorrelation(int TS, int length);        //finds the self correlation of given time series, assuming that groupVector contains the groupIDs TS subsequences belong to
	void longestCorrNaive(int TS1, int TS2, int minLength, double corrThreshold);
	void longestCORAL(int TS1, int TS2, int minLength);		//CORAL Operation of longest length subsequence
	void selfCorrNaive(int TS, int length, double corrThreshold);
	void naiveGroupCorr(double threshold, int length, bool positiveOnly);
	void getGroupsSeq(int length, double threshold, bool positiveOnly);	//CORAL operation of getting groups of correlated sequences
    //variables
	vector <osubsequenceGroup> groupSeq;     //vector containing grouped subsequences for Naive
    int groupCounter;       //maintains the the total number of groups so far.
    int N;  //number of time series
    int L;  //length of time series
    double ST;            //Similarity threshold
    double min;         //min value in data
    double max;         //max value in data for normalization
    int centroidDist[timeSeriesN][timeSeriesN];     //Dc distance
    timeEntry Time[timeSeriesL];       //Highest level length index
    double timeSeries[timeSeriesN][timeSeriesL];        //raw data
    bool sequencesGrouped[timeSeriesN][timeSeriesL];  // if entry is true then sequence is placed in group
    ifstream inputFile;         //input data file
    ifstream queryFile;         //input query file
    ofstream timeFile;          //output the Global Time index
    ofstream groupFile;         //output file for groups and its attributes
    ofstream groupResults;
    vector<Group> groupArray;   //  vector containing all the clusters
    vector<Group> tempCentroids;    //temporary centroids created while grouping
    int distanceMethod;         //distance metric selected
    string GroupFilePath;       //path for groups file
    string timeFilePath;        //path for Time file
    bool centroidFlag[timeSeriesN][timeSeriesL];     //it is true if this is selected as temp centroid    
	int seqExplored;	//number of sequences explored by CORAL, use this number for pruning
    double normFactor;      //defines the normalized factor for Euclidean
    //online operations variables
    ifstream inputTimeFile;         //input Time data file
    ifstream groupInFile;         //input Group file
    ofstream queryF;            //query data to be used for PAA
    ofstream queryN;
    double *Q;      //query data
    int queryLength;          //size of query data
    vector<double> tempQ;       //vector to hold query
    double globalDist;
	/************************************************************
	************************* Muzammil **************************
	*************************************************************/

	std::vector<std::vector<std::vector<bool>>> SequenceIndexMap;


	std::vector<std::vector<bool>> GetSequenceIndexSlices(int j);
	std::vector<int> PrintGroupCitizenship(int i, int j);
	int GetLongestMatchingSsGroups(int TimeSeries1, int TimeSeries2, int MinLength, std::vector<int> &CommonGroupVec, int &SsLength);
	__int64 MakeKey(int Key1, int Key2);			// Merges key1 and key2 into one unique key independencet of their order
	void MakeInterRepCorrMap(int Length);
	void PrintGroupSeqCount(ofstream &fileHandle, int CurrentLength);

	// Results variables
	int CORALBestTSId = 0;
	int CORALBestTSStart = 0;
	int CORALBestTSEnd = 0;
	double CORALBestCorr = 0;


	int NaiveBestTSId = 0;
	int NaiveBestTSStart = 0;
	int NaiveBestTSEnd = 0;
	double NaiveBestCorr = 0;


	bool IsGroupArrayInMem = false;

	int CORALnCandidateClusters = 0;
	
};
