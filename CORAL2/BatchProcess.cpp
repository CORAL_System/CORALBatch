
// @brief: This file contains code for batch processing the queries

#include "BatchProcess.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <stdexcept>
#include "Groups.h"
#include <time.h>
#include <stdlib.h>

BatchProcess::BatchProcess(void *groupObj) :m_groupObj(groupObj)
{
}

BatchProcess::~BatchProcess()
{
}


bool BatchProcess::SetInputCommFileAddr(char * _InputCommFileAddr)
{
	std::string buffer(_InputCommFileAddr);
	InputCommFileAddr = buffer;
	
	return true;
}


bool BatchProcess::SetTimeSeriesN(int _TimeSeriesN)
{
	TimeSeriesN = _TimeSeriesN;
	return true;
}


bool BatchProcess::SetTimeSeriesL(int _TimeSeriesL)
{
	TimeSeriesL = _TimeSeriesL;
	return true;
}


//Added by Ramoza
/******************************/
bool BatchProcess::SetST(double groupST)
{
	ST = groupST;
	return true;
}
bool BatchProcess::SetNumberRuns(int queryRuns)
{
	NumQueryProcess = queryRuns;
	return true;

}

bool BatchProcess::SetInputDataSet(char *inputfilename)
{
	std::string buffer(inputfilename);
	InputDatasetAddr = buffer;
	return true;
}
bool BatchProcess::SetOutputFile(char *outputfilename)
{
	std::string buffer(outputfilename);
	OutputFileAddr = buffer;
	return true;
}
bool BatchProcess::SetChoice(int _Choice)
{
	Choice = _Choice;
	return true;
}
bool BatchProcess::SetDelta(double _Delta)
{
	Delta = _Delta;
	return true;
}

//This function will run batch operations like groups of correlated Sequence, self correlation and longest
void BatchProcess::runOtherOperations(void)
{
	// Read the input parameters
	//ReadParams();

	// Check for bad params
	//	BadParamCheck();

	// Open output file
	h_OutputFile.open(OutputFileAddr);

	RaiseError(h_OutputFile.is_open() == false, "Error: Unable to open output file");

	h_OutputFile << "Index" << DL << "Length" << DL << "GCS-CorrThres" << DL << "GCS-NaiveTime" << DL;
	h_OutputFile << "GCS-CORALTime" << DL << "TS" << DL << "SC-Naive" << DL << "SC-CORAL";

	//<< DL << "TS1" << DL << "TS2" << DL;
	//h_OutputFile << "minLength" << DL;
	//h_OutputFile << "Naive Longest" << DL << "CORAL Longest" << DL ;
	h_OutputFile << std::endl;
	int minLengthSet = TimeSeriesL-70;
	//run the number of queries	

	for (int k = 0; k < NumQueryProcess; k++)
	{
		CurrentQueryIdx = k;
		cout << "k " << k << endl;
		//first run the groups of correlated operation
		//generate the length
		int length = rand2(minLengthSet, TimeSeriesL-1);
		//cout << "length "<<length << endl;
		//cin >> minLengthSet;
		//cout << "Length " << length << endl;
		//generate random threshold
		double corrThreshold = randCorr();
		RunNaiveCorrGroups(length, corrThreshold);
		std::cout << "Naive Corr Groups completed" << std::endl;
		//run CORAL
		RunCORALCorrGroups(length, corrThreshold);
		h_OutputFile << k << DL << length << DL << corrThreshold << DL << NaiveElaspedTime << DL << CORALElaspedTime << DL;
		std::cout << "CORAL Corr Groups completed" << std::endl;


		//run the self correlation
		//generate random TS 
		int TS = rand2(0, TimeSeriesN-1);
		//self corr for Naive
		RunNaiveSelfCorr(TS, length);
		std::cout << "Naive Self Corr completed" << std::endl;

		//self corr for CORAL
		RunCORALSelfCorr(TS, length);
		h_OutputFile << TS << DL << NaiveElaspedTime << DL << CORALElaspedTime << DL;
		h_OutputFile << std::endl;
		/*
		//write the output
		h_OutputFile << k << DL << length << corrThreshold << DL << NaiveElaspedTime << DL<< CORALElaspedTime << DL;
		//run the self correlation
		//generate random TS
		int TS = rand2(0, TimeSeriesN);
		//self corr for Naive
		RunNaiveSelfCorr(TS, length);
		//self corr for CORAL
		RunCORALSelfCorr(TS, length);
		//write the output
		h_OutputFile << TS << DL << NaiveElaspedTime << DL << CORALElaspedTime << DL;
		*/
		//run the longest correlation operation
		//generate random TS 
		/*LONGEST commented out
		int TS1 = rand2(0, TimeSeriesN);
		//generate random TS
		int TS2 = rand2(0, TimeSeriesN);	//need to check also if two TS are not same
		//run longest corr for naive
		RunNaiveLongestCorr(TS1, TS2, length);
		//run longest corr for CORAL
		RunCORALLongestCorr(TS1, TS2, length);
		//write the output
		h_OutputFile << TS1 << DL << TS2 << DL << length << NaiveElaspedTime << DL << CORALElaspedTime;
		*/

	}

}

//added by Ramoza
//runs Self correlation for CORAL
void BatchProcess::RunCORALSelfCorr(int TS, int length)
{
	/*
	// Return if CORAL process flag has been false
	if (doCORALProcess == false)
	{
	return;
	}

	// Run preprocessing
	if (((GroupOperation*)m_groupObj)->IsGroupArrayInMem == false)
	{
	// Run preprocessing if GroupArray is not in memory
	((GroupOperation*)m_groupObj)->ST = ST;
	((GroupOperation*)m_groupObj)->groupOp(Choice, Delta, length);
	}
	*/
	clock_t start1 = clock();
	((GroupOperation*)m_groupObj)->selfCorrelation(TS, length);
	CORALElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);
}
void BatchProcess::RunNaiveSelfCorr(int TS, int length)
{
	/*
	// Exit if control param is false
	if (doNAIVEProcess == false)
	{
		return;
	}
	*/

	

	// Run the naive calculation function
	clock_t start1 = clock();

	double corrThreshold = (1 - ((ST*ST) ));
	std::cout << "This is running" << std::endl;
	((GroupOperation*)m_groupObj)->selfCorrNaive(TS, length, corrThreshold);
	NaiveElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);

}
void BatchProcess::RunCORALLongestCorr(int TS1, int TS2, int length)
{
	/*
	// Return if CORAL process flag has been false
	if (doCORALProcess == false)
	{
	return;
	}
	bool corrFound = false;
	int minLength = length;
	int currentLength = TimeSeriesL;
	// Run preprocessing
	//if (((GroupOperation*)m_groupObj)->IsGroupArrayInMem == false)
	//	{
	// Run preprocessing if GroupArray is not in memory
	((GroupOperation*)m_groupObj)->ST = ST;
	((GroupOperation*)m_groupObj)->groupOp(Choice, Delta, currentLength);
	//	}
	while (corrFound == false && currentLength >= minLength)
	{
	corrFound = ((GroupOperation*)m_groupObj)->longestCORAL(TS1, TS2, currentLength);
	}
	*/
}
void BatchProcess::RunNaiveLongestCorr(int TS1, int TS2, int length)
{
	/*
	// Exit if control param is false
	if (doNAIVEProcess == false)
	{
	return;
	}

	// Run the naive calculation function
	clock_t start1 = clock();
	//run from longest length
	bool corrFound = false;
	int minLength = length;
	int currentLength = TimeSeriesL;
	double corrThreshold = (1 - (ST*ST) / 2);
	while (corrFound == false && currentLength >= minLength)
	{
	corrFound = ((GroupOperation*)m_groupObj)->longestCorrNaive(TS1, TS2, currentLength, corrThreshold);
	}
	NaiveElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);
	*/
}
//Added by Ramoza
//runs CORAL correlated groups
void BatchProcess::RunCORALCorrGroups(int length, double corrThreshold)
{
	/*
	// Return if CORAL process flag has been false
	if (doCORALProcess == false)
	{
	return;
	}
	*/
	// Run preprocessing
	//if (((GroupOperation*)m_groupObj)->IsGroupArrayInMem == false)
	//	{
	// Run preprocessing if GroupArray is not in memory
	((GroupOperation*)m_groupObj)->ST = ST;
	((GroupOperation*)m_groupObj)->groupOp(Choice, Delta, length);
	//	}

	clock_t start1 = clock();
	((GroupOperation*)m_groupObj)->getGroupsSeq(length, corrThreshold, false);
	CORALElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);

}
void BatchProcess::RunNaiveCorrGroups(int length, double corrThreshold)
{
	/*
	// Exit if control param is false
	if (doNAIVEProcess == false)
	{
	return;
	}
	*/
	// Run the naive calculation function
	clock_t start1 = clock();
	((GroupOperation*)m_groupObj)->naiveGroupCorr(corrThreshold, length, false);
	NaiveElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);


}

//generates random corr threshold between 0.5-1
double BatchProcess::randCorr()
{
	return ((double)rand() / (double)RAND_MAX)*0.5 + 0.5;

}

//RAMOZA CODE ENDED
/********************************************/



void BatchProcess::Run(void)
{

	// Initialize before actual processing
	Initialize();

	for (int k = 0; k < NumQueryProcess; k++)
	{
		CurrentQueryIdx = k;

		// Load the query to be processed
		LoadQuery();

		// Run CORAL on currently loaded query
		RunCORAL();

		// Run NAIVE method on current query
		RunNaive();

		// Write the output
		WriteOutput();

		DoWrapup();
	}

	Finalize();
}


void BatchProcess::ReadParams()
{
	std::ifstream h_InputCommFile;		// input command file reading object


	//Extract relevant info from command file
	h_InputCommFile.open(InputCommFileAddr);
	RaiseError(h_InputCommFile.is_open() == false, "Error opening input command file.");

	h_InputCommFile >> InputDatasetAddr;
	h_InputCommFile >> OutputFileAddr;
	h_InputCommFile >> OutputDirAddr;
	h_InputCommFile >> MinLenInsideQuery;
	h_InputCommFile >> f_QueryType;
	h_InputCommFile >> NumQueryProcess;
	h_InputCommFile >> doCORALProcess;
	h_InputCommFile >> doNAIVEProcess;
	h_InputCommFile >> ST;
	h_InputCommFile >> Choice;
	h_InputCommFile >> Delta;

	// Read outside query params only if needed
	if (f_QueryType == CORAL_OUTSIDEQUERY)
	{
		h_InputCommFile >> OutQueryFileAddrTmp;
		h_InputCommFile >> OutQueryFileStartIdx;
		h_InputCommFile >> OutQueryFileEndIdx;
		h_InputCommFile >> OutQueryFileExt;	// extension is without dot

		// Update # of queries to be processed
		NumQueryProcess = OutQueryFileEndIdx - OutQueryFileStartIdx + 1;
	}



	h_InputCommFile.close();

}

void BatchProcess::BadParamCheck()
{
	RaiseError(MinLenInsideQuery<1, "Error: Invalid minimum length query");
	RaiseError(f_QueryType<CORAL_MINQUERYFLAG || f_QueryType>CORAL_MAXQUERYFLAG, "Error: Invalid Query Flag");
	RaiseError(NumQueryProcess<1, "Error: Invalid number of queries to be processed");
	RaiseError(Choice < 0 || Choice>2, "Error: Invalid choice of grouping algorithm");

	RaiseError(OutQueryFileStartIdx < 0, "Error: Invalid outside query file start index");
	RaiseError(OutQueryFileEndIdx < 0, "Error: Invalid outside query file start index");
	RaiseError(OutQueryFileStartIdx > OutQueryFileEndIdx, "Error: Start index is greater than end index");

}

void BatchProcess::RaiseError(bool DecisionFlag, std::string ErrStr)
{
	if (DecisionFlag == true)
	{
		std::cout <<"\n" << ErrStr << std::endl;
		abort();
	}
}

void BatchProcess::RunCORAL()
{

	// Return if CORAL process flag has been false 
	if (doCORALProcess == false)
	{
		return;
	}

	// Run preprocessing
	if (((GroupOperation*)m_groupObj)->IsGroupArrayInMem == false)
	{
		// Run preprocessing if GroupArray is not in memory
		((GroupOperation*)m_groupObj)->ST = ST;
		((GroupOperation*)m_groupObj)->groupOp(Choice, Delta, QueryLenVec[CurrentQueryIdx]);
	}


	// online query matching
	int queryLength = ((GroupOperation*)m_groupObj)->tempQ.size();
	((GroupOperation*)m_groupObj)->znormSequenceS(((GroupOperation*)m_groupObj)->tempQ);
	clock_t start1 = clock();
	((GroupOperation*)m_groupObj)->kSimilarnew(queryLength);
	CORALElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);


}

void BatchProcess::RunNaive()
{
	// Exit if control param is false
	if (doNAIVEProcess == false)
	{
		return;
	}

	// Run the naive calculation function
	clock_t start1 = clock();
	((GroupOperation*)m_groupObj)->springCalculation();
	NaiveElaspedTime = (clock() - start1) / double(CLOCKS_PER_SEC);
}

void BatchProcess::GenerateRandomQueries()
{
	// Reserve appropriate space
	QueryTSIdVec.resize(NumQueryProcess);
	QueryLenVec.resize(NumQueryProcess);
	QueryOffsetVec.resize(NumQueryProcess);

	// If queries are of outside type then there is no need to generate random numbers
	if (f_QueryType == CORAL_OUTSIDEQUERY)
	{
		for (int k = 0; k < NumQueryProcess; k++)
		{
			QueryTSIdVec[k] = 0;
			QueryLenVec[k] = 0;
			QueryOffsetVec[k] = 0;
		}
		return;
	}

	// Initialize the seed
	srand(time(0));

	// Decide random subsequnces 
	for (int k = 0; k < NumQueryProcess; k++)
	{
		// get a random time series

		//{
			// This code deprecated
			//QueryTSIdVec[k] = (rand() % timeSeriesN); // pick a number 0 to N-1
			//QueryLenVec[k] = (rand() % (timeSeriesL-2)) + 1; // pick a number 1 to L-2
			//QueryOffsetVec[k] = (rand() % (timeSeriesL - QueryLenVec[k])); // pick a number 0 to L-l
		// }

		QueryTSIdVec[k] = rand2(0, TimeSeriesN-1); // pick a number 0 to N-1
		QueryLenVec[k] = rand2(MinLenInsideQuery,TimeSeriesL-2); // pick a number 1 to L-2
		QueryOffsetVec[k] = rand2(0,TimeSeriesL - QueryLenVec[k]-1); // pick a number 0 to L-l-1

		// Selected Query Display
		//std::cout << QueryTSIdVec[k] << ": " << QueryOffsetVec[k] << "-" << QueryOffsetVec[k] + QueryLenVec[k] << std::endl;

	}
}

int BatchProcess::rand2(int a, int b)
{
	RaiseError(a > b, "Error in selecting random inside queries");

	// a random number is generted from a to b
	int x = b - a;
	return ((rand() % (x + 1)) + a);
}

void BatchProcess::LoadQuery()
{
	if (f_QueryType == CORAL_INSIDEQUERY)
	{
		// std::cout << "Inside queries will be processed" << std::endl;
		int ID = QueryTSIdVec[CurrentQueryIdx];
		int Start = QueryOffsetVec[CurrentQueryIdx];
		int End = Start + QueryLenVec[CurrentQueryIdx];
		
		// Load the query
		((GroupOperation*)m_groupObj)->loadQueryTS(ID, Start, End);



		// Write the query to disk
		std::ofstream h_InsideQuery;
		std::string Address = OutputDirAddr + "\\" + "InsideQuery"+ std::to_string(CurrentQueryIdx) + ".txt";
		h_InsideQuery.open(Address);
		
		//if file is not open, then error is raised and program is stopped
		RaiseError(h_InsideQuery.is_open() == false, "Error writing inside query to disk. \nHint: Check if output directory exits.");
		

		int CurrentQueryLength = QueryLenVec[CurrentQueryIdx];
		for (int k = 0; k < CurrentQueryLength; k++)
		{
			h_InsideQuery << ((GroupOperation*)m_groupObj)->tempQ[k] << " ";
		}
		h_InsideQuery << ((GroupOperation*)m_groupObj)->tempQ[CurrentQueryLength];

		h_InsideQuery.close();

	}


	else if (f_QueryType == CORAL_OUTSIDEQUERY)
	{
		// std::cout << "Outside queries will be processed" << std::endl;

		std::string QueryFileAddr = OutQueryFileAddrTmp + std::to_string(OutQueryFileStartIdx+CurrentQueryIdx) + "." + OutQueryFileExt;
		

		std::ifstream h_OutsideQuery;
		h_OutsideQuery.open(QueryFileAddr);

		RaiseError(h_OutsideQuery.is_open() == false,"Error opening outside query file.");


		double buf;
		((GroupOperation*)m_groupObj)->tempQ.clear();
		while (h_OutsideQuery.eof() == false)
		{
			h_OutsideQuery >> buf;
			((GroupOperation*)m_groupObj)->tempQ.push_back(buf);
		}

		h_OutsideQuery.close();
		QueryLenVec[CurrentQueryIdx] = ((GroupOperation*)m_groupObj)->tempQ.size()-1;
	}

	else
	{
		std::cout << "Invalid Query type" << std::endl;
		abort();
	}

}

void BatchProcess::WriteOutput()
{
	int k = 0;

	int CORALBestTSId = ((GroupOperation*)m_groupObj)->CORALBestTSId;
	int CORALBestTSStart = ((GroupOperation*)m_groupObj)->CORALBestTSStart;
	int CORALBestTSEnd = ((GroupOperation*)m_groupObj)->CORALBestTSEnd;
	double CORALBestCorr = ((GroupOperation*)m_groupObj)->CORALBestCorr;
	int CORALnCandidateClusters = ((GroupOperation*)m_groupObj)->CORALnCandidateClusters;

	int NaiveBestTSId = ((GroupOperation*)m_groupObj)->NaiveBestTSId;
	int NaiveLBestTSStart = ((GroupOperation*)m_groupObj)->NaiveBestTSStart;
	int NaiveBestTSEnd = ((GroupOperation*)m_groupObj)->NaiveBestTSEnd;
	double NaiveBestCorr = ((GroupOperation*)m_groupObj)->NaiveBestCorr;
	
	CORALError = abs(abs(NaiveBestCorr) - abs(CORALBestCorr));

	// for inside query, print CurrentQueryIdx; otherwise add the offset given by outside query start index (OutQueryFileStartIdx)
	(f_QueryType == CORAL_INSIDEQUERY) ? k = CurrentQueryIdx: k = CurrentQueryIdx + OutQueryFileStartIdx;

	
	h_OutputFile << k << DL << QueryTSIdVec[CurrentQueryIdx] << DL << QueryOffsetVec[CurrentQueryIdx] << DL << QueryLenVec[CurrentQueryIdx] << DL;
	h_OutputFile << CORALBestTSId << DL << CORALBestTSStart << DL << CORALBestTSEnd << DL << CORALBestCorr << DL << CORALElaspedTime << DL << CORALError <<DL;
	h_OutputFile << CORALnCandidateClusters << DL;
	h_OutputFile << NaiveBestTSId << DL << NaiveLBestTSStart << DL << NaiveBestTSEnd << DL << NaiveBestCorr << DL << NaiveElaspedTime;
	h_OutputFile << std::endl;

}

void BatchProcess::Initialize()
{
	//Read the input command file
	// printf("\nEnter input command file address:\n");
	// std::cin >> InputCommFileAddr;

	// Read the input parameters
	ReadParams();

	// Check for bad params
	BadParamCheck();

	// Generate random inside queries
	GenerateRandomQueries();


	// Open output file
	h_OutputFile.open(OutputFileAddr);

	RaiseError(h_OutputFile.is_open()==false,"Error: Unable to open output file");

	h_OutputFile << "Index" << DL << "Query ID" << DL << "Query Start" << DL << "Query Length" << DL;
	h_OutputFile << "CORAL TSID" << DL << "CORAL TStart" << DL << "CORAL TEnd" << DL << "CORAL Corr" << DL << "CORAL Time (s)" << DL << "Error" <<DL;
	h_OutputFile << "CORAL no. Candidate Clusters" << DL;
	h_OutputFile << "Naive TSID" << DL << "Naive TStart" << DL << "Naive TEnd" << DL << "Naive Corr" << DL << "Naive Time (s)" << DL;
	h_OutputFile << std::endl;

}

void BatchProcess::DoWrapup()
{
	// Update the user
	std::cout << CurrentQueryIdx + 1 << "/" << NumQueryProcess << " queries completed." << std::endl;

	// Update running time counters
	TotalCORALElaspedTime += CORALElaspedTime;
	TotalNaiveElaspedTime += NaiveElaspedTime;
	TotalCORALError += CORALError;
}

void BatchProcess::Finalize()
{
	// Compute summary
	double MeanCORALElaspedTime = TotalCORALElaspedTime / NumQueryProcess;
	double MeanNaiveElaspedTime = TotalNaiveElaspedTime / NumQueryProcess;
	double MeanCORALError = TotalCORALError / NumQueryProcess;
	double Accuracy = (1-MeanCORALError)*100;

	// Write summary to output file
	h_OutputFile << std::endl;

	h_OutputFile << "Average" << DL << "" << DL << "" << DL << "" << DL;
	h_OutputFile << "" << DL << "" << DL << "" << DL << "" << DL << MeanCORALElaspedTime << DL << MeanCORALError << DL;
	h_OutputFile << "" << DL;
	h_OutputFile << "" << DL << "" << DL << "" << DL << "" << DL << MeanNaiveElaspedTime << DL;
	h_OutputFile << std::endl;

	h_OutputFile << "Accuracy" << DL << "" << DL << "" << DL << "" << DL;
	h_OutputFile << "" << DL << "" << DL << "" << DL << "" << DL << "" << DL << Accuracy << DL;
	h_OutputFile << "" << DL;
	h_OutputFile << "" << DL << "" << DL << "" << DL << "" << DL << "" << DL;
	h_OutputFile << std::endl;



	// Close the output file
	h_OutputFile.close();
	h_OutputFile.is_open() ? std::cout << "WARNING:Outputfile not closed\a" << std::endl : std::cout<<"";

}