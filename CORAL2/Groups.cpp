
#include "Groups.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <cmath>
#include <map>
#include <set>
#include <algorithm>
#include <numeric>
#include <limits>
#include "ControlParam.h"
#include <vector>

#define INF 1e20       //Pseudo Infitinte number for this code
int totalIterations = 0;
GroupOperation::GroupOperation(double STinput, int Ndata, int Ldata)
{
	//myGroup->seqCentroidDist[1]
	groupCounter = 0;
	ST = STinput;
	N = Ndata;
	L = Ldata;

	seqExplored = 0;
	globalDist = INF;





}
GroupOperation::~GroupOperation()
{

}
double distFuncG(double x, double y)
{

	return (pow((x - y), 2));

}

//reads the time series data from input file and store in structure
//n is number of time series and L is length of time series, skip is true if first number of each time series should be skipped
int GroupOperation::readFile(const char * Filename, int number, int length)
{
	inputFile.open(Filename); //Opening the file
	N = number;
	L = length;
	min = 10000;
	max = 0;
	//double skipNum;
	if (!inputFile)
	{
		cout << "there's something wrong with the file!\n"; //Throw exception
		return -1;
	}
	else
	{
		//read the input file into time series structure
		//Read numbers from file into the array
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j<L; j++)
			{
				inputFile >> timeSeries[i][j];
				if (timeSeries[i][j]<min)
					min = timeSeries[i][j];
				if (timeSeries[i][j]>max)
					max = timeSeries[i][j];
			}
		}
		inputFile.close();

		return 1;
	}



}
//prints the time series
void GroupOperation::printTS()
{
	//cout<<"Printing Timeseries"<<endl;
	for (int i = 0; i<N; i++)
	{
		for (int j = 0; j<L; j++)
		{
			cout << timeSeries[i][j] << " ";
		}
		cout << endl;
	}

}
//prints all the times and asociated information (group ids, count of groups)
void GroupOperation::printTime()
{

	timeFile.open(timeFilePath.c_str());
	//cout<<endl<<endl;
	for (int l = 1; l<L; l++)
	{

		//     cout<<"Time intervals: "<<l<<" "<<m<<endl;
		timeFile << "T " << l << " Groups " << Time[l].groupid.size() << endl;
		int groupCount = Time[l].groupid.size();
		for (int k = 0; k<groupCount; k++)
		{
			timeFile << "G" << " " << Time[l].groupid[k] << " ";

		}
		timeFile << endl;
	}
	timeFile.close();
}
//prints alll groups with associated information
void GroupOperation::printGroups()
{


	groupFile.open(GroupFilePath.c_str());
	cout << "Total Groups " << groupArray.size() << endl;
	groupFile << "Total" << " " << groupArray.size() << endl;
	groupFile << "Min " << min << " " << "Max " << max << endl;
	int TScount = groupArray.size();
	for (int i = 0; i<TScount; i++)
	{
		//       cout<<"Group "<<groupArray[i].id<<endl;
		groupFile << "G " << groupArray[i].id << " " << groupArray[i].count << endl;

		for (int k = 0; k<groupArray[i].seqObj.size(); k++)
		{
			groupFile << groupArray[i].seqObj[k].id << " " << groupArray[i].seqObj[k].startT << " " << groupArray[i].seqObj[k].endT << " ";
		}
		groupFile << endl;

		groupFile << "CentroidLen " << groupArray[i].centroid.size() << " " << groupArray[i].centroidID << " " << groupArray[i].centroidStart << endl;
		for (int k = 0; k<groupArray[i].centroid.size(); k++)
		{
			groupFile << groupArray[i].centroid[k] << " ";
		}
		groupFile << endl;
	}

	groupFile.close();

}
//picks multiple groups in each iteration.
//optimized reducing time
//CORAL method for grouping, length is grouping sequences for the length specified, delta determines hwo much variation from sqrt (N) seq group can be picked
void GroupOperation::qualityGroupingMultipleOpti(int length, double delta)
{
	//first get the initial set of centroid
	for (int i = 0; i<N; i++)
	{
		for (int j = 0; j<L - length; j++)
		{
			sequencesGrouped[i][j] = false;
			centroidFlag[i][j] = false;
		}
	}
	//first get the initial set of centroid
	int centroidCount = 0;
//	int iterationCount = 0;
	normFactor = sqrt(length + 1);		//as length =1 means it has 2 points
	vector<subsequence> tempSeqObj;

	double seqMean = 0.0;
	double seqStd = 0.0;

	double EDist = 0;
	int seqLeft = N*(L - length);

	int centPick = (int)sqrt(seqLeft);
	int seqTotal = centPick;       //these many need to be in group

	vector<double> tempSZN; //temp z-nromalized time series

	while (seqLeft>0)
	{
	//	iterationCount++;
//		totalIterations++;

		for (int i = 0; i<L - length; i++)
		{
			//Check each subsequence
			for (int j = 0; j<N; j++)
			{
				//check if this sequence is not yet grouped
				if (sequencesGrouped[j][i] == false)
				{
					if (centroidCount <= centPick)   //if sqrt N centroids are picked exit from here
					{
						//make first centroid
						if (centroidCount == 0)
						{
							Group *newGroup = new Group();
							subsequence *newSeq = new subsequence;
							newGroup->count = 1;
							//z-normalize this centroid
							for (int k = i; k <= i + length; k++)
								newGroup->centroid.push_back(timeSeries[j][k]);
							znormSequence(newGroup->centroid, seqMean, seqStd);

							newSeq->id = j;
							newSeq->startT = i;
							newGroup->centroidID = j;
							newGroup->centroidStart = i;
							newGroup->mean = seqMean; //setting mean of centroid
							newGroup->std = seqStd;   //setting std for centroid
							newSeq->mean = seqMean;
							newSeq->std = seqStd;
							newSeq->endT = length + newSeq->startT;
							centroidFlag[j][i] = true;

							newGroup->seqObj.push_back(*newSeq);
							tempCentroids.push_back(*newGroup);
							delete newSeq;
							centroidCount++;
							delete newGroup;
						}
						else    //check with other centroids, while checking place in centroids too
						{
							int currentCentroids = centroidCount;
							bool distFlag = false;
							EDist = 0;
							tempSZN.clear();
							for (int l = 0; l <= length; l++)
							{
								tempSZN.push_back(timeSeries[j][i + l]);
							}
							znormSequence(tempSZN, seqMean, seqStd);
							for (int k = 0; k<currentCentroids; k++)
							{
								//timeseries needs to be z-normalized
								for (int l = 0; l <= length; l++)
									EDist += (tempSZN[l] - tempCentroids[k].centroid[l])*(tempSZN[l] - tempCentroids[k].centroid[l]);
								EDist = sqrt(EDist);
								//   tempSZN.clear();
								if (EDist<(ST*(normFactor)))   //it is within ST of any centroid
								{
									distFlag = true;
									//place this sequence in this temp centroid if it is with ST/2
									if (EDist<(ST*normFactor) / 2)   //it is within ST/2 of centroid
									{
										subsequence *newSeq = new subsequence;
										newSeq->id = j;
										newSeq->startT = i;
										newSeq->endT = i + length;
										newSeq->mean = seqMean;
										newSeq->std = seqStd;
										//update the bit vector
										tempCentroids[k].seqObj.push_back(*newSeq);
										delete newSeq;
										tempCentroids[k].count++;
										break;
									}

									//    tempSZN.clear();
									

								}
								EDist = 0;

							}
							if (distFlag == false)
							{
								//make this a new centroid
								//make new group
								Group *nGroup = new Group();
								nGroup->id = 0;//groupCounter++;
								nGroup->count = 1;
								subsequence *newSeq = new subsequence;
								newSeq->id = j;
								newSeq->startT = i;
								newSeq->endT = i + length;
								newSeq->mean = seqMean;
								newSeq->std = seqStd;
								nGroup->centroidID = j;
								nGroup->mean = seqMean;
								nGroup->std = seqStd;
								nGroup->centroidStart = i;
								centroidFlag[j][i] = true;
								//here centroid needs to be z-normalized
								for (int k = 0; k <= length; k++)
									nGroup->centroid.push_back(tempSZN[k]);
								//  tempSZN.clear();
								nGroup->seqObj.push_back(*newSeq);
								// nGroup->seqObj.push_back(*newSeq);
								tempCentroids.push_back(*nGroup);
								delete newSeq;
								centroidCount++;
								delete nGroup;
							}


						}
					}
					else
					{
						//sequences are still left to be picked
						if (centroidCount == 0)
						{
							Group *newGroup = new Group();
							subsequence *newSeq = new subsequence;
							newGroup->count = 1;
							//here centroid needs to be z-normalized
							for (int k = i; k <= i + length; k++)
								newGroup->centroid.push_back(timeSeries[j][k]);
							znormSequence(newGroup->centroid, seqMean, seqStd);
							newGroup->mean = seqMean; //setting mean of centroid
							newGroup->std = seqStd;   //setting std for centroid
							newSeq->mean = seqMean;
							newSeq->std = seqStd;
							newSeq->id = j;
							newSeq->startT = i;
							newGroup->centroidID = j;
							newGroup->centroidStart = i;
							newSeq->endT = length + newSeq->startT;
							centroidFlag[j][i] = true;

							newGroup->seqObj.push_back(*newSeq);
							tempCentroids.push_back(*newGroup);
							delete newSeq;
							centroidCount++;
							delete newGroup;
						}
						else    //check with other centroids
						{
							int currentCentroids = centroidCount;
							bool distFlag = false;
							EDist = 0;
							tempSZN.clear();
							for (int l = 0; l <= length; l++)
							{
								tempSZN.push_back(timeSeries[j][i + l]);
							}
							znormSequence(tempSZN, seqMean, seqStd);
							for (int k = 0; k<currentCentroids; k++)
							{
								for (int l = 0; l <= length; l++)
									EDist += (tempSZN[l] - tempCentroids[k].centroid[l])*(tempSZN[l] - tempCentroids[k].centroid[l]);
								EDist = sqrt(EDist);
								if (EDist<(ST*(normFactor)))   //it is within ST of any centroid
								{
									distFlag = true;
									if (EDist<(ST*normFactor) / 2)   //it is within ST/2 of centroid
									{
										subsequence *newSeq = new subsequence;
										newSeq->id = j;
										newSeq->startT = i;
										newSeq->endT = i + length;
										newSeq->mean = seqMean;
										newSeq->std = seqStd;
										//update the bit vector
										tempCentroids[k].seqObj.push_back(*newSeq);
										delete newSeq;
										tempCentroids[k].count++;
									}

									//  tempSZN.clear();
									break;

								}
								EDist = 0;

							}
							if (distFlag == false)
							{
								//make this a new centroid
								//make new group
								Group *nGroup = new Group();
								nGroup->id = 0;//groupCounter++;
								nGroup->count = 1;
								subsequence *newSeq = new subsequence;
								newSeq->id = j;
								newSeq->startT = i;
								newSeq->endT = i + length;
								nGroup->centroidID = j;
								nGroup->centroidStart = i;
								centroidFlag[j][i] = true;
								//here centroid needs to be z-normalized
								for (int k = i; k <= i + length; k++)
									nGroup->centroid.push_back(timeSeries[j][k]);
								znormSequence(nGroup->centroid, seqMean, seqStd);
								nGroup->mean = seqMean; //setting mean of centroid
								nGroup->std = seqStd;   //setting std for centroid
								newSeq->mean = seqMean;
								newSeq->std = seqStd;
								nGroup->seqObj.push_back(*newSeq);								
								tempCentroids.push_back(*nGroup);								
								delete newSeq;
								centroidCount++;
								delete nGroup;
							}


						}

					}

				}

			}

		}

		//at this point initial temp centroids have been picked and sequences have been placed too
		/*
		//assign each subsequence to the cluster
		//    bool groupFound=false;
		for (int i = 0; i<L - length; i++)
		{
			//Check each subsequence
			for (int j = 0; j<N; j++)
			{
				for (int l = 0; l <= length; l++)
				{
					tempSZN.push_back(timeSeries[j][i + l]);
				}
				znormSequence(tempSZN, seqMean, seqStd);
				for (int k = 0; k<centroidCount; k++)
				{
					if (sequencesGrouped[j][i] == false)
					{
						if (centroidFlag[j][i] == true)
							break;       //this sequence is already a centroid
						else
						{
							EDist = 0;
							//here timeseries needs to be z-normalized

							for (int l = 0; l <= length; l++)
								EDist += (tempSZN[l] - tempCentroids[k].centroid[l])*(tempSZN[l] - tempCentroids[k].centroid[l]);
							EDist = sqrt(EDist);
							if (EDist<(ST*normFactor) / 2)   //it is within ST/2 of centroid and it is not same as centroid
							{
								subsequence *newSeq = new subsequence;
								newSeq->id = j;
								newSeq->startT = i;
								newSeq->endT = i + length;
								newSeq->mean = seqMean;
								newSeq->std = seqStd;
								//update the bit vector
								tempCentroids[k].seqObj.push_back(*newSeq);
								delete newSeq;
								tempCentroids[k].count++;



							}

						}

					}
				}
				tempSZN.clear();

			}
		}
		*/
		//at this point sequences have been placed in groups
		//at this point pick all the groups having closest to sqrt N sequences and put in final group
		int bestGroupIndex = 0;
		int bestGroupDev = (2 ^ 31) - 1;

		bool groupFlag = false;
		for (int i = 0; i<centroidCount; i++)
		{
			//this should be cent pick
			int seqCount = abs(tempCentroids[i].count - seqTotal);
			//try to avoid picking groups with only 1 member
			if (tempCentroids[i].count == 1)        //no other group has been picked
			{
				if (bestGroupDev == (2 ^ 31) - 1)   //no other group has been picked
				{
					if (bestGroupDev>seqCount)
					{
						bestGroupDev = seqCount;
						bestGroupIndex = i;
					}
				}
			}
			else
			{
				if (bestGroupDev>seqCount)
				{
					bestGroupDev = seqCount;
					bestGroupIndex = i;
				}
			}
			if (seqCount <= ceil((delta / 100)*seqTotal))
			{
				//this groups should be added
				groupFlag = true;
				tempCentroids[i].id = groupCounter++;
				centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart] = true;
				groupArray.push_back(tempCentroids[i]);
				centPick--;				
				//add this group to the the list of groups for this time interval
				Time[length].groupid.push_back(tempCentroids[i].id);
				
				int tempCount = tempCentroids[i].count;
				for (int k = 0; k<tempCount; k++)
				{

					int TSindex = tempCentroids[i].seqObj[k].id;
					int start = tempCentroids[i].seqObj[k].startT;

					if (sequencesGrouped[TSindex][start] == false)
					{

						sequencesGrouped[TSindex][start] = true;
						seqLeft--;

					}

				}

				/*
				tempSeqObj.clear();

				for (int j = 0; j<tempCount; j++)
				{

					int TSindex = tempCentroids[i].seqObj[j].id;
					int start = tempCentroids[i].seqObj[j].startT;

					if (sequencesGrouped[TSindex][start] == false)
					{

						sequencesGrouped[TSindex][start] = true;
						seqLeft--;
						subsequence *tempNewSeq = new subsequence;
						tempNewSeq->id = TSindex;
						tempNewSeq->startT = start;
						tempNewSeq->endT = tempCentroids[i].seqObj[j].endT;
						tempNewSeq->mean = tempCentroids[i].seqObj[j].mean;
						tempNewSeq->std = tempCentroids[i].seqObj[j].std;
						tempSeqObj.push_back(*tempNewSeq);
						delete tempNewSeq;
					}


				}
				//add this group to final groups list
				//should not add if 0 sequences are there
				if (tempSeqObj.size() != 0)
				{
					tempCentroids[i].seqObj.clear();    //erase all
					tempCentroids[i].seqObj = tempSeqObj;
					tempCentroids[i].count = tempSeqObj.size();
					tempCentroids[i].id = groupCounter++;
					//   tempCentroids[i].centroidMean=std::accumulate(tempCentroids[i].centroid.begin(), tempCentroids[i].centroid.end(), 0.0)/tempCentroids[i].centroid.size();

					centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart] = true;
					//    cout<<"Seq inside group "<<tempCentroids[i].count<<endl;
					groupArray.push_back(tempCentroids[i]);
					//   groupResults<<tempCentroids[i].count;
					//   groupResults<<',';

					//one more centroid has ben picked
					centPick--;
					//     cout<<" , "<<tempCentroids[i].count;
					//add this group to the the list of groups for this time interval
					Time[length].groupid.push_back(tempCentroids[i].id);
				}
				*/

			}
			else
			{
				//this centroid is not picked
				centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart] = false;
			}


		}
		if (groupFlag == false)
		{
			//none of the group has delta sqrt N sequences, promote the closest one
			tempCentroids[bestGroupIndex].id = groupCounter++;
			centroidFlag[tempCentroids[bestGroupIndex].centroidID][tempCentroids[bestGroupIndex].centroidStart] = true;
			groupArray.push_back(tempCentroids[bestGroupIndex]);
			centPick--;
			//add this group to the the list of groups for this time interval
			Time[length].groupid.push_back(tempCentroids[bestGroupIndex].id);
			int tempCount = tempCentroids[bestGroupIndex].count;
			for (int j = 0; j<tempCount; j++)
			{

				int TSindex = tempCentroids[bestGroupIndex].seqObj[j].id;
				int start = tempCentroids[bestGroupIndex].seqObj[j].startT;

				if (sequencesGrouped[TSindex][start] == false)
				{

					sequencesGrouped[TSindex][start] = true;
					seqLeft--;

				}

			}
		}
		tempCentroids.clear();
		centroidCount = 0;

	}
}



//picks multiple groups in each iteration.
//CORAL method for grouping, length is grouping sequences for the length specified, delta determines hwo much variation from sqrt (N) seq group can be picked
void GroupOperation::qualityGroupingMultiple(int length, double delta)
{
	//first get the initial set of centroid
	for (int i = 0; i<N; i++)
	{
		for (int j = 0; j<L - length; j++)
		{
			sequencesGrouped[i][j] = false;
			centroidFlag[i][j] = false;
		}
	}
	//first get the initial set of centroid
	int centroidCount = 0;
	int iterationCount = 0;
	normFactor = sqrt(length + 1);		//as length =1 means it has 2 points
	vector<subsequence> tempSeqObj;

	double seqMean = 0.0;
	double seqStd = 0.0;

	double EDist = 0;
	int seqLeft = N*(L - length);

	int centPick = (int)sqrt(seqLeft);
	int seqTotal = centPick;       //these many need to be in group

								   //   cout<<"Total Sequences to be Grouped "<<seqLeft;
								   //writing in group File
								   /*
								   groupResults<<seqLeft;
								   groupResults<<',';
								   groupResults<<centPick;
								   groupResults<<',';
								   */
	vector<double> tempSZN; //temp z-nromalized time series

	while (seqLeft>0)
	{
		iterationCount++;
		totalIterations++;

		for (int i = 0; i<L - length; i++)
		{
			//Check each subsequence
			for (int j = 0; j<N; j++)
			{
				//check if this sequence is not yet grouped
				if (sequencesGrouped[j][i] == false)
				{
					if (centroidCount <= centPick)   //if sqrt N centroids are picked exit from here
					{
						//make first centroid
						if (centroidCount == 0)
						{
							Group *newGroup = new Group();
							subsequence *newSeq = new subsequence;
							newGroup->count = 1;
							//z-normalize this centroid
							for (int k = i; k <= i + length; k++)
								newGroup->centroid.push_back(timeSeries[j][k]);
							znormSequence(newGroup->centroid, seqMean, seqStd);

							newSeq->id = j;
							newSeq->startT = i;
							newGroup->centroidID = j;
							newGroup->centroidStart = i;
							newGroup->mean = seqMean; //setting mean of centroid
							newGroup->std = seqStd;   //setting std for centroid
							newSeq->mean = seqMean;
							newSeq->std = seqStd;
							newSeq->endT = length + newSeq->startT;
							centroidFlag[j][i] = true;

							newGroup->seqObj.push_back(*newSeq);
							tempCentroids.push_back(*newGroup);
							delete newSeq;
							centroidCount++;
							delete newGroup;
						}
						else    //check with other centroids
						{
							int currentCentroids = centroidCount;
							bool distFlag = false;
							EDist = 0;
							tempSZN.clear();
							for (int l = 0; l <= length; l++)
							{
								tempSZN.push_back(timeSeries[j][i + l]);
							}
							znormSequence(tempSZN, seqMean, seqStd);
							for (int k = 0; k<currentCentroids; k++)
							{
								//timeseries needs to be z-normalized
								for (int l = 0; l <= length; l++)
									EDist += (tempSZN[l] - tempCentroids[k].centroid[l])*(tempSZN[l] - tempCentroids[k].centroid[l]);
								EDist = sqrt(EDist);
								//   tempSZN.clear();
								if (EDist<(ST*(normFactor)))   //it is within ST of any centroid
								{
									distFlag = true;
									//    tempSZN.clear();
									break;

								}
								EDist = 0;

							}
							if (distFlag == false)
							{
								//make this a new centroid
								//make new group
								Group *nGroup = new Group();
								nGroup->id = 0;//groupCounter++;
								nGroup->count = 1;
								subsequence *newSeq = new subsequence;
								newSeq->id = j;
								newSeq->startT = i;
								newSeq->endT = i + length;
								newSeq->mean = seqMean;
								newSeq->std = seqStd;
								nGroup->centroidID = j;
								nGroup->mean = seqMean;
								nGroup->std = seqStd;
								nGroup->centroidStart = i;
								centroidFlag[j][i] = true;
								//here centroid needs to be z-normalized
								for (int k = 0; k <= length; k++)
									nGroup->centroid.push_back(tempSZN[k]);
								//  tempSZN.clear();
								nGroup->seqObj.push_back(*newSeq);
								// nGroup->seqObj.push_back(*newSeq);
								tempCentroids.push_back(*nGroup);
								delete newSeq;
								centroidCount++;
								delete nGroup;
							}


						}
					}
					else
					{
						//sequences are still left to be picked
						if (centroidCount == 0)
						{
							Group *newGroup = new Group();
							subsequence *newSeq = new subsequence;
							newGroup->count = 1;
							//here centroid needs to be z-normalized
							for (int k = i; k <= i + length; k++)
								newGroup->centroid.push_back(timeSeries[j][k]);
							znormSequence(newGroup->centroid, seqMean, seqStd);
							newGroup->mean = seqMean; //setting mean of centroid
							newGroup->std = seqStd;   //setting std for centroid
							newSeq->mean = seqMean;
							newSeq->std = seqStd;
							newSeq->id = j;
							newSeq->startT = i;
							newGroup->centroidID = j;
							newGroup->centroidStart = i;
							newSeq->endT = length + newSeq->startT;
							centroidFlag[j][i] = true;

							newGroup->seqObj.push_back(*newSeq);
							tempCentroids.push_back(*newGroup);
							delete newSeq;
							centroidCount++;
							delete newGroup;
						}
						else    //check with other centroids
						{
							int currentCentroids = centroidCount;
							bool distFlag = false;
							EDist = 0;
							tempSZN.clear();
							for (int l = 0; l <= length; l++)
							{
								tempSZN.push_back(timeSeries[j][i + l]);
							}
							znormSequence(tempSZN, seqMean, seqStd);
							for (int k = 0; k<currentCentroids; k++)
							{
								for (int l = 0; l <= length; l++)
									EDist += (tempSZN[l] - tempCentroids[k].centroid[l])*(tempSZN[l] - tempCentroids[k].centroid[l]);
								EDist = sqrt(EDist);
								if (EDist<(ST*(normFactor)))   //it is within ST of any centroid
								{
									distFlag = true;
									//  tempSZN.clear();
									break;

								}
								EDist = 0;

							}
							if (distFlag == false)
							{
								//make this a new centroid
								//make new group
								Group *nGroup = new Group();
								nGroup->id = 0;//groupCounter++;
								nGroup->count = 1;
								subsequence *newSeq = new subsequence;
								newSeq->id = j;
								newSeq->startT = i;
								newSeq->endT = i + length;
								nGroup->centroidID = j;
								nGroup->centroidStart = i;
								centroidFlag[j][i] = true;
								//here centroid needs to be z-normalized
								for (int k = i; k <= i + length; k++)
									nGroup->centroid.push_back(timeSeries[j][k]);
								znormSequence(nGroup->centroid, seqMean, seqStd);
								nGroup->mean = seqMean; //setting mean of centroid
								nGroup->std = seqStd;   //setting std for centroid
								newSeq->mean = seqMean;
								newSeq->std = seqStd;
								nGroup->seqObj.push_back(*newSeq);
								// nGroup->seqObj.push_back(*newSeq);
								tempCentroids.push_back(*nGroup);
								//    tempSZN.clear();
								delete newSeq;
								centroidCount++;
								delete nGroup;
							}


						}

					}

				}

			}

		}

		//at this point initial temp centroids have been picked
		//assign each subsequence to the cluster
		//    bool groupFound=false;
		for (int i = 0; i<L - length; i++)
		{
			//Check each subsequence
			for (int j = 0; j<N; j++)
			{
				for (int l = 0; l <= length; l++)
				{
					tempSZN.push_back(timeSeries[j][i + l]);
				}
				znormSequence(tempSZN, seqMean, seqStd);
				for (int k = 0; k<centroidCount; k++)
				{
					if (sequencesGrouped[j][i] == false)
					{
						if (centroidFlag[j][i] == true)
							break;       //this sequence is already a centroid
						else
						{
							EDist = 0;
							//here timeseries needs to be z-normalized

							for (int l = 0; l <= length; l++)
								EDist += (tempSZN[l] - tempCentroids[k].centroid[l])*(tempSZN[l] - tempCentroids[k].centroid[l]);
							EDist = sqrt(EDist);
							if (EDist<(ST*normFactor) / 2)   //it is within ST/2 of centroid and it is not same as centroid
							{
								subsequence *newSeq = new subsequence;
								newSeq->id = j;
								newSeq->startT = i;
								newSeq->endT = i + length;
								newSeq->mean = seqMean;
								newSeq->std = seqStd;
								//update the bit vector
								tempCentroids[k].seqObj.push_back(*newSeq);
								delete newSeq;
								tempCentroids[k].count++;



							}

						}

					}
				}
				tempSZN.clear();

			}
		}
		//at this point sequences have been placed in groups
		//at this point pick all the groups having closest to sqrt N sequences and put in final group
		int bestGroupIndex = 0;
		int bestGroupDev = (2 ^ 31) - 1;

		bool groupFlag = false;
		for (int i = 0; i<centroidCount; i++)
		{
			//this should be cent pick
			int seqCount = abs(tempCentroids[i].count - seqTotal);
			//try to avoid picking groups with only 1 member
			if (tempCentroids[i].count == 1)        //no other group has been picked
			{
				if (bestGroupDev == (2 ^ 31) - 1)   //no other group has been picked
				{
					if (bestGroupDev>seqCount)
					{
						bestGroupDev = seqCount;
						bestGroupIndex = i;
					}
				}
			}
			else
			{
				if (bestGroupDev>seqCount)
				{
					bestGroupDev = seqCount;
					bestGroupIndex = i;
				}
			}
			/*
			if(bestGroupDev>seqCount)
			{
			bestGroupDev=seqCount;
			bestGroupIndex=i;
			}
			*/
			if (seqCount <= ceil((delta / 100)*seqTotal))
			{
				groupFlag = true;
				//this groups should be added
				//first iterate over all its sequences to remove any overlapping sequences
				//iterate over al the sequences in this group and replace from original set
				int tempCount = tempCentroids[i].count;
				tempSeqObj.clear();

				for (int j = 0; j<tempCount; j++)
				{

					int TSindex = tempCentroids[i].seqObj[j].id;
					int start = tempCentroids[i].seqObj[j].startT;

					if (sequencesGrouped[TSindex][start] == false)
					{

						sequencesGrouped[TSindex][start] = true;
						seqLeft--;
						subsequence *tempNewSeq = new subsequence;
						tempNewSeq->id = TSindex;
						tempNewSeq->startT = start;
						tempNewSeq->endT = tempCentroids[i].seqObj[j].endT;
						tempNewSeq->mean = tempCentroids[i].seqObj[j].mean;
						tempNewSeq->std = tempCentroids[i].seqObj[j].std;
						tempSeqObj.push_back(*tempNewSeq);
						delete tempNewSeq;
					}


				}
				//add this group to final groups list
				//should not add if 0 sequences are there
				if (tempSeqObj.size() != 0)
				{
					tempCentroids[i].seqObj.clear();    //erase all
					tempCentroids[i].seqObj = tempSeqObj;
					tempCentroids[i].count = tempSeqObj.size();
					tempCentroids[i].id = groupCounter++;
					//   tempCentroids[i].centroidMean=std::accumulate(tempCentroids[i].centroid.begin(), tempCentroids[i].centroid.end(), 0.0)/tempCentroids[i].centroid.size();

					centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart] = true;
					//    cout<<"Seq inside group "<<tempCentroids[i].count<<endl;
					groupArray.push_back(tempCentroids[i]);
					//   groupResults<<tempCentroids[i].count;
					//   groupResults<<',';

					//one more centroid has ben picked
					centPick--;
					//     cout<<" , "<<tempCentroids[i].count;
					//add this group to the the list of groups for this time interval
					Time[length].groupid.push_back(tempCentroids[i].id);
				}

			}
			else
			{
				//this centroid is not picked
				centroidFlag[tempCentroids[i].centroidID][tempCentroids[i].centroidStart] = false;
			}


		}
		if (groupFlag == false)
		{
			//none of the group has delta sqrt N sequences, promote the closest one
			tempCentroids[bestGroupIndex].id = groupCounter++;
			centroidFlag[tempCentroids[bestGroupIndex].centroidID][tempCentroids[bestGroupIndex].centroidStart] = true;
			//   tempCentroids[bestGroupIndex].centroidMean=std::accumulate(tempCentroids[bestGroupIndex].centroid.begin(), tempCentroids[bestGroupIndex].centroid.end(), 0.0)/tempCentroids[bestGroupIndex].centroid.size();
			//   cout<<"Seq inside group "<<tempCentroids[bestGroupIndex].count<<endl;
			groupArray.push_back(tempCentroids[bestGroupIndex]);
			// groupResults<<tempCentroids[bestGroupIndex].count;
			//  groupResults<<',';
			centPick--;
			//    cout<<" , "<<tempCentroids[bestGroupIndex].count;
			//add this group to the the list of groups for this time interval
			Time[length].groupid.push_back(tempCentroids[bestGroupIndex].id);
			int tempCount = tempCentroids[bestGroupIndex].count;
			for (int j = 0; j<tempCount; j++)
			{

				int TSindex = tempCentroids[bestGroupIndex].seqObj[j].id;
				int start = tempCentroids[bestGroupIndex].seqObj[j].startT;

				if (sequencesGrouped[TSindex][start] == false)
				{

					sequencesGrouped[TSindex][start] = true;
					seqLeft--;

				}

			}
		}
		tempCentroids.clear();
		centroidCount = 0;

	}
}



//groups the time series
int GroupOperation::groupOp(int method, double delta = 10.0, int length)
{

	groupArray.clear();
	totalIterations = 0;
	//intilize again the groupCounter
	groupCounter = 0;
	//  groupResults<<"Length"<<','<<"TotalSeq"<<','<<"Exp"<<','<<"Group Seq"<<','<<"Groups"<<endl;
	//now group the time series for all intervals

	//string GroupingOutputResDir = R"(E:\WPI\Semester1\DR_598\CORALDatasets\)";
	//ofstream fH_nGroupDev(GroupingOutputResDir + "nGroupDev.txt");
	//ofstream fH_GroupDev(GroupingOutputResDir + "GroupDev.txt");

	//int ConstLength = 410;

	for (int j = 1; j<L; j++)
	 //for (int j = ConstLength-1; j <= ConstLength-1; j++)
	//for (int j = 480; j <= 512; j++)
	{
		//   cout<<"Length "<<j<<endl;

		if (length > 0) {j = length;}

		Time[j].groupid.clear();
		//   groupResults<<j<<',';
		if (method == 0)
			qualityGroupingMultipleOpti(j, delta);	//optimized version
		if (method == 1)
			qualityGroupingMultiple(j, delta);
		else if (method == 2)
			groupNewOp(j);
		cout << " Groups generated at length " << j << " -> " << Time[j].groupid.size() << endl;
		
		// int TotalSS = N*(L - j);
		// length | total subsequences to be groups | Expected no. of grousp (E) | Observed no. of groups (O) | deviation abs(O-E)/E
		// fH_nGroupDev << j << " " << TotalSS << " " << round(sqrt(double(TotalSS))) << " " << Time[j].groupid.size() << " " << abs(Time[j].groupid.size() / round(sqrt(double(TotalSS))) - 1) << endl;
		// sPrintGroupSeqCount(fH_GroupDev,j);
		

			SequenceIndexMap.resize(L);
			std::vector<std::vector<bool>>	Map = GetSequenceIndexSlices(j);
			SequenceIndexMap.at(j) = Map;

			MakeInterRepCorrMap(j);
		//	__int64 Key = MakeKey(0, 2); 			// Compute key for (i,j)

		//	cout<<"Corr between first two reps "<<Time[j].InterRepCorrMap[Key]; 	// insert in map
			
		if (length > 0) { break; }

	}
	/*
	cout << "Muzammil code " << endl;
	PrintGroupCitizenship(1, 10);
	cout << "Ramoza code " << endl;
	printTSGroups(1, 10);
	*/

	//printing all the groups
	//  groupResults.close();

	//    cout<<"Total Iterations"<<totalIterations<<endl;
	// cout << "Total Groups " << groupArray.size() << endl;
	// fH_nGroupDev.close();
	// fH_GroupDev.close();

	return groupArray.size();


}
//Greedy grouping method, changed so that centroid doesn't change
void GroupOperation::groupNewOp(int length)
{
	//Time[length].count=0;
	Group *newGroup;//=new Group();
	subsequence *newSeq = new subsequence;
	double EDDist = 0;
	int bestGroupIndex = 0;
	double bsf = INF;       //best so far distance
	int startTime = 0;
	bestGroupIndex = 0;
	vector<double> tempSeries;
	vector<double> tempSZN; //temp z-nromalized time series
	double seqMean = 0.0;
	double seqStd = 0.0;
	normFactor =sqrt(length+1);    //normalized euclidean factor
									 //itarte over time intervals of length given
	for (int i = 0; i<N; i++)
	{
		for (int j = 0; j<L - length; j++)
		{
			sequencesGrouped[i][j] = false;
			
		}
	}
	for (int i = 0; i<L - length; i++)
	{
		//pick subsequences
		for (int j = 0; j<N; j++)
		{
			if (Time[length].groupid.size() == 0)  //no groups created yet
			{
				newGroup = new Group();
				//make the first group

				newGroup->id = groupCounter++;
				//add this group to the the list of groups for this time interval
				Time[length].groupid.push_back(newGroup->id);
				newGroup->count = 1;

				for (int i = startTime; i <= length; i++)
				{
					//first new centroid is first time series
					newGroup->centroid.push_back(timeSeries[0][i]);
					//  newGroup->sum.push_back(timeSeries[0][i]);
				}
				//znormalize centroid
				znormSequence(newGroup->centroid, seqMean, seqStd);

				newGroup->mean = seqMean; //setting mean of centroid
				newGroup->std = seqStd;   //setting std for centroid
				newSeq->mean = seqMean;
				newSeq->std = seqStd;
				newSeq->id = j;
				newSeq->startT = i;
				newSeq->endT = length;

				newGroup->seqObj.push_back(*newSeq);
				newGroup->centroidID = j;
				newGroup->centroidStart = i;
				//     newGroup->centroidMean=std::accumulate( newGroup->centroid.begin(), newGroup->centroid.end(), 0.0)/newGroup->centroid.size();
				delete newSeq;
				groupArray.push_back(*newGroup);
				delete newGroup;

			}
			else
			{
				//check with the groups
				bsf = INF;
				int currentGroups = Time[length].groupid.size();  //get the number of groups for this length
				tempSZN.clear();
				for (int l = 0; l <= length; l++)
				{
					tempSZN.push_back(timeSeries[j][i + l]);
				}
				znormSequence(tempSZN, seqMean, seqStd);
				for (int k = 0; k<currentGroups; k++)
				{
					EDDist = 0;
					int groupID = Time[length].groupid[k];

					for (int l = 0; l <= length; l++)
					{
						//compare time series with centroid one at a time
						EDDist += (tempSZN[l] - groupArray[groupID].centroid[l])*(tempSZN[l] - groupArray[groupID].centroid[l]);
					}

					EDDist = sqrt(EDDist);

					if (EDDist<bsf)
					{
						bsf = EDDist;
						bestGroupIndex = groupID;
					}

				}
				if (bsf <= (ST*normFactor) / 2)
				{
					groupArray[bestGroupIndex].count++; //increase the count of TS in this group

														//add this subsequence in the group
					subsequence *newSeq = new subsequence;
					newSeq->id = j;
					newSeq->startT = i;
					newSeq->endT = i + length;
					newSeq->mean = seqMean;
					newSeq->std = seqStd;					
					groupArray[bestGroupIndex].seqObj.push_back(*newSeq);
					delete newSeq;
					int tempCount = groupArray[bestGroupIndex].count;
					for (int m = 0; m<tempCount; m++)
					{

						int TSindex = groupArray[bestGroupIndex].seqObj[m].id;
						int start = groupArray[bestGroupIndex].seqObj[m].startT;

						if (sequencesGrouped[TSindex][start] == false)
						{
							sequencesGrouped[TSindex][start] = true;

						}

					}
				}
				else
				{
					//make new group
					Group *nGroup = new Group();
					nGroup->id = groupCounter++;
					subsequence *newSeq = new subsequence;
					newSeq->id = j;
					newSeq->startT = i;
					newSeq->endT = i + length;
					newSeq->mean = seqMean;
					newSeq->std = seqStd;
					nGroup->mean = seqMean;
					nGroup->std = seqStd;

					nGroup->seqObj.push_back(*newSeq);
					nGroup->centroidID = j;
					nGroup->centroidStart = i;

					//add this group id to this time interval groups
					Time[length].groupid.push_back(nGroup->id);


					nGroup->count = 1;

					//this is the centroid of this group

					for (int k = 0; k <= length; k++)
					{
						//first new centroid is first time series
						nGroup->centroid.push_back(tempSZN[k]);
						//  nGroup->sum.push_back(timeSeries[j][k]);
					}

					tempSZN.clear();
					int tempCount = nGroup->count;
					for (int m = 0; m<tempCount; m++)
					{

						int TSindex = nGroup->seqObj[m].id;
						int start = nGroup->seqObj[m].startT;

						if (sequencesGrouped[TSindex][start] == false)
						{
							sequencesGrouped[TSindex][start] = true;							

						}

					}
					//push this group in groups vector
					
					groupArray.push_back(*nGroup);
					delete newSeq;
					delete nGroup;
				}
			}
		}
	}
}
void GroupOperation::znormSequence(vector<double> &t1, double &mean, double &std)
{
	double gMean = 0;   //mean of the sequences
	double gStd = 0;     //std of sequences
	double sum = 0;
	double scalingFactor = 4.0;


	//  cout<<"Z-normalize sequence"<<endl;
	//here in z-normalize also compute the max in the z-normalize sequences and in end divide each with that max
	double max = 0;   //initializing with first element

	gStd = 0;
	sum = 0;
	gMean = 0;
	//calculate sum of sequence
	for (int j = 0; j<t1.size(); j++)
	{
		sum += t1[j];
	}
	//calculating mean of this sequence
	gMean = sum / t1.size();
	//calculating std of these sequences
	for (int j = 0; j<t1.size(); j++)
	{

		gStd += pow(t1[j] - gMean, 2);
	}
	gStd = sqrt((gStd / t1.size()));
	if (gStd == 0)		//if std is 0 set it to ver small number to avoid division by 0.
		gStd = std::numeric_limits<double>::epsilon();
	//z-normalize this sequence

	//max=fabs((max-gMean)/gStd);
	for (int j = 0; j<t1.size(); j++)
	{
		t1[j] = (t1[j] - gMean) / gStd;
		if (fabs(t1[j])>max)
			max = fabs(t1[j]);    //update the max
								  //t1[j]=t1[j]/max;

	}
	//now divide each term in z-normalized sequence with the max value so that its range is from [-1,1]
	//	if (max < 1)	//check if max value is < 1 add 1 to it, so that range remains between [-1,1] of z-normalize sequence
	max = max + scalingFactor;	//scaling factor
					//	else
					//		max = max * sqrt(t1.size());
					//	else
					//	max = max * 2;


					//scaling the z-normalized sequence so range stays between -1 and 1
	for (int j = 0; j<t1.size(); j++)
	{
		t1[j] = t1[j] / max;
	}
	mean = gMean;
	std = gStd;
}

// ONLINE OPERATIONS
void GroupOperation::znormSequenceS(vector<double> &t1)
{
	double gMean = 0;   //mean of the sequences
	double gStd = 0;     //std of sequences
	double sum = 0;


	gStd = 0;
	sum = 0;
	gMean = 0;
	//calculate sum of sequence
	for (int j = 0; j<t1.size(); j++)
	{
		sum += t1[j];
	}
	//calculating mean of this sequence
	gMean = sum / t1.size();
	//calculating std of these sequences
	for (int j = 0; j<t1.size(); j++)
	{
		gStd += pow(t1[j] - gMean, 2);
	}
	gStd = sqrt((gStd / t1.size()));
	if (gStd == 0)		//if std is 0 set it to very small number to avoid division by 0.
		gStd = std::numeric_limits<double>::epsilon();
	//z-normalize this sequence

	for (int j = 0; j<t1.size(); j++)
	{
		t1[j] = (t1[j] - gMean) / gStd;

	}

}

//reads the query file
//type =true means normalize and false means running for Spring don't normalize
void GroupOperation::readQueryFile(int m, const char * Filename, bool type)
{
	tempQ.clear();
	// Array for keeping the query data
	Q = (double *)malloc(sizeof(double)*m);
	queryLength = m;
	queryFile.open(Filename);
	//read the query data and normalize at the same time
	for (int i = 0; i<m; i++)
	{
		queryFile >> Q[i];
		tempQ.push_back(Q[i]);
	}
	//queryN.close();
	delete Q;
	queryFile.close();
}

//finds k similar time series, query Flag=true query sequence is given
//timeFlag=true time interval Exact, false means Any



void GroupOperation::loadQueryTS(int TSid, int startTS, int endTS)
{
	//load subsequence if TS in query object
	tempQ.clear();
	//also write this query in file as well
	queryF.open("E:/WPI/Semester1/DR_598/CORALDatasets/Earthquakes/Inside/queryInside.txt");
	for (int i = startTS; i <= endTS; i++)
	{
		tempQ.push_back(timeSeries[TSid][i]);
		queryF << timeSeries[TSid][i];
		if (i != endTS)
			queryF << " ";
		//  cout<<timeSeries[TSid][i]<<" ";
	}
	queryF.close();
}
//correlation function for naive method, it z-normalizes the sequences first and then compute with same formula of dot product
double GroupOperation::correlation(vector<double>& t1, vector<double>& t2)
{
	//z-normalize the two sequences
	znormSequenceS(t1);
	znormSequenceS(t2);
	/*
	double meanX=0;
	double meanY=0;
	double stdX=0;
	double stdY=0;
	*/
	double corr = 0;
	/*
	meanX= std::accumulate( t1.begin(), t1.end(), 0.0)/t1.size();
	meanY=std::accumulate( t2.begin(), t2.end(), 0.0)/t2.size();
	//calculate standard deviations
	for(int i=0;i<t1.size();i++)
	stdX+=pow(t1[i]-meanX,2);
	stdX=sqrt(stdX/t1.size());
	for(int j=0;j<t2.size();j++)
	stdY+=pow(t2[j]-meanY,2);
	stdY=sqrt(stdY/t2.size());
	*/
	//calculate correlation measure

	for (int i = 0; i<t1.size(); i++)
	{
		//      double term1=(t1[i]-meanX)/stdX;
		//    double term2=(t2[i]-meanY)/stdY;
		double term = t1[i] * t2[i];
		corr = corr + term;
	}
	corr = corr / t1.size();
	return corr;
}

//correlation computation function for CORAL
//just a dot product between the two values, sequences passed are already z-normalized
double GroupOperation::CORALcorrelation(const vector<double>& t1, const vector<double>& t2)
{
	double corr = 0;

	//calculate correlation measure

	for (int i = 0; i<t1.size(); i++)
	{
		double term = t1[i] * t2[i];
		corr = corr + term;
	}
	corr = corr / t1.size();
	return corr;
}

//naive method of finding the best match, compares with every subsequence
void GroupOperation::springCalculation()
{
	queryN.open("C:/Data/log.txt");		//just of log purposes
	double bsf = 0;
	// double actualCorr;
	int bsfIndex;       //index of bsf time series
	double currentCorr = 0;
	int bestIntervalS;
	int bestIntervalE;
	vector<double> temp;    //temporary subsequence
							//calculate mean of query sequence
							//    double meanQ=std::accumulate(tempQ.begin(),tempQ.end(),0.0);
							//get each subsequence
	for (int j = 1; j<L; j++)
	{
		for (int l = 0, m = l + j; m<L; l++, m++)
		{
			for (int i = 0; i<N; i++)
			{
				temp.clear();
				//copy  the subsequence in temp
				for (int p = 0; p <= m - l; p++)
				{
					temp.push_back(timeSeries[i][l + p]);

				}

				//check if subsequence is same length as query

				if (temp.size() == tempQ.size())
				{

					currentCorr = correlation(temp, tempQ);	//sequences are z-normalized inside the correlation function

					if (fabs(currentCorr)>fabs(bsf))		//found best so far correlation
					{
						//for logging copy data in file, only this data is considered best compared with query
						for (int i = 0; i < temp.size(); i++)
						{
							queryN << temp[i];
							queryN << " ";

						}
						queryN << endl;

						bsf = currentCorr;
						bsfIndex = i;        //best time series index
						bestIntervalS = l;   //record the best interval
						bestIntervalE = m;
					}
					//clear the temp array
					temp.clear();
					currentCorr = 0;
				}
				else
					temp.clear();

			}
		}
	}

	//print the closest sequence
	cout << "Best Closest sequence Corr<";
	cout << "TS " << bsfIndex << " Interval " << bestIntervalS << " " << bestIntervalE << " Corr " << bsf << endl;
	NaiveBestTSId = bsfIndex;
	NaiveBestTSStart = bestIntervalS;
	NaiveBestTSEnd = bestIntervalE;
	NaiveBestCorr = bsf;

	temp.clear();
	queryN.close();
}

//reads time information from the file containing the count of groups in each time interval
void GroupOperation::readTime(const char * Filename)
{
	/*
	inputTimeFile.open(Filename);
	//read till the end of file
	string temp;
	int length;
	int count;
	while(inputTimeFile)
	{

	inputTimeFile>>temp;    //read T
	inputTimeFile>>length;  //read length interval
	inputTimeFile>>temp;        //read Groups
	inputTimeFile>>count;  //count of groups in this interval

	for(int i=0;i<count;i++)
	{
	inputTimeFile>>temp;    //read G
	int id;
	inputTimeFile>>id;
	Time[length].groupid.push_back(id);
	}
	if(length==L-1)
	break;
	}
	inputFile.close();
	*/
}
//reads the groups attributes from a file and saves in group structure
void GroupOperation::readGroups(const char * Filename)
{
	/*
	groupInFile.open(Filename);
	string temp;
	//int startT;
	// int endT;
	int count;
	int totalcount;
	subsequence *newSeq;
	Group *newGroup;//=new Group();
	groupInFile>>temp;        //reading total
	groupInFile>>totalcount;
	groupInFile>>temp;        //reading min
	groupInFile>>min;
	groupInFile>>temp;        //reading max
	groupInFile>>max;
	while(groupInFile)
	{
	newGroup=new Group();
	groupInFile>>temp;        //reading G
	groupInFile>>newGroup->id;    //group id
	groupInFile>>newGroup->count; //count of TS
	//read Sequences in this group
	for(int i=0;i<newGroup->count;i++)
	{
	newSeq=new subsequence;
	groupInFile>>newSeq->id;
	groupInFile>>newSeq->startT;
	groupInFile>>newSeq->endT;
	newGroup->seqObj.push_back(*newSeq);
	delete newSeq;
	}
	//read centroid
	groupInFile>>temp;        //reading Centroid
	groupInFile>>count;   //centroid length
	groupInFile>>newGroup->centroidID;
	groupInFile>>newGroup->centroidStart;
	//    groupFile>>newGroup->centroidMean;
	//newGroup->centroidLen=count;
	for(int i=0;i<count;i++)
	{
	double value;
	groupInFile>>value;
	newGroup->centroid.push_back(value);
	}
	groupArray.push_back(*newGroup);
	if(newGroup->id==totalcount-1)
	{
	//all groups read
	delete newGroup;
	groupInFile.close();
	return;
	}
	delete newGroup;

	}
	groupInFile.close();
	*/
}
//based on new quality grouping find similar with specific length
double GroupOperation::kSimilarnew(int length)
{
	// **************************************** BRIEF ****************************************  
	// CORAL_ONLINE_S1_CHECKNCLUSTER==0 runs legacy code that only checks the top cluster for matching subsequence
	// CORAL_ONLINE_S1_CHECKNCLUSTER==1 runs new code that checks the top 3 cluster for matching subsequence


#if CORAL_ONLINE_S1_CHECKMULTICLUSTER==0
	double bsf = 0;
	seqExplored = 0;
	int bsfIndex=0;       //index of bsf centroid
	int bsfTSIndex;
	double dist = 0;
	double currentDist = 0;
	int numGroups = Time[length - 1].groupid.size();       //get the number of groups for this length
														   //z-normalize the query seq

														   //   double meanQ=std::accumulate(tempQ.begin(),tempQ.end(),0.0);
	for (int i = 0; i<numGroups; i++)
	{
		//check the closest group
		int groupID = Time[length - 1].groupid[i];
		//check if the group is of same length as query


		//compute point wise corrrelation distance
		//centroids are alread z-normalized so no need to z-normalize again
		currentDist = correlation(groupArray[groupID].centroid, tempQ);
		seqExplored++;
		if (((int)fabs(currentDist) * 100) / 100 == 1.0)    //perfect correlation found, stop
		{
#if CORAL_ONLINE_S1_PRINTRESULTS ==1
			cout << groupArray[groupID].centroidID << "[" << groupArray[groupID].centroidStart << "," << groupArray[groupID].centroidStart + length - 1 << "] " << "corr " << currentDist << endl;
#endif
			return currentDist;     //found the centroid with corr=1 , return
		}
		if (fabs(currentDist)>fabs(bsf))
		{
			bsf = currentDist;
			bsfIndex = groupID;
		}

	}

	//at this point have the best group
	//compute with each sequence present in that group
	bsf = 0;
	vector<double> tempSeries;
	bsfTSIndex = 0;
	currentDist = 0;
	int bestStart = 0;
	int bestEnd = 0;
	dist = 0;
	for (int i = 0; i<groupArray[bsfIndex].seqObj.size(); i++)
	{
		int seqID = groupArray[bsfIndex].seqObj[i].id;
		int seqStart = groupArray[bsfIndex].seqObj[i].startT;
		int seqEnd = groupArray[bsfIndex].seqObj[i].endT;
		//copy this time series in tempSeries
		//z-normalize while copying the data
		for (int j = seqStart; j <= seqEnd; j++)
		{
			tempSeries.push_back((timeSeries[seqID][j] - groupArray[bsfIndex].seqObj[i].mean) / groupArray[bsfIndex].seqObj[i].std);
		}

		//compute point wise correlation distance
		dist = correlation(tempSeries, tempQ);
		seqExplored++;
		if (((int)fabs(dist) * 100) / 100 == 1.0)       //found perfect correlation ,stop
		{
#if CORAL_ONLINE_S1_PRINTRESULTS ==1
			cout << seqID << "[" << seqStart << "," << seqEnd << "] " << "corr " << dist << endl;
#endif
			CORALBestTSId = seqID;
			CORALBestTSStart = seqStart;
			CORALBestTSEnd = seqEnd;
			CORALBestCorr = dist;
			return dist;

		}
		if (fabs(bsf)<fabs(dist))
		{
			bsf = dist;
			bsfTSIndex = seqID;
			bestStart = seqStart;
			bestEnd = seqEnd;
		}
		tempSeries.clear();
		dist = 0;


	}
#if CORAL_ONLINE_S1_PRINTRESULTS ==1
	cout << bsfTSIndex << "[" << bestStart << "," << bestEnd << "] " << " Corr " << bsf << endl;
#endif
	CORALBestTSId = bsfTSIndex;
	CORALBestTSStart = bestStart;
	CORALBestTSEnd = bestEnd;
	CORALBestCorr = bsf;
	return bsf;
#endif // CORAL_ONLINE_S1_CHECKMULTICLUSTER==0

#if CORAL_ONLINE_S1_CHECKMULTICLUSTER==1

	// std::cout << "*********** Looking into" << " N " <<"Clusters ***********" << std::endl;

	double bsf = 0;
	seqExplored = 0;
	int bsfIndex = 0;       //index of bsf centroid
	int bsfTSIndex;
	double dist = 0;
	double currentDist = 0;
	int numGroups = Time[length - 1].groupid.size();       //get the number of groups for this length
														   //z-normalize the query seq

	//std::vector CentroidCorrVec<double, numGroups>;
	std::vector<double> CentroidCorrVec(numGroups,0);



														   //   double meanQ=std::accumulate(tempQ.begin(),tempQ.end(),0.0);
	for (int i = 0; i<numGroups; i++)
	{
		//check the closest group
		int groupID = Time[length - 1].groupid[i];
		//check if the group is of same length as query


		//compute point wise corrrelation distance
		//centroids are alread z-normalized so no need to z-normalize again
		currentDist = correlation(groupArray[groupID].centroid, tempQ);
		seqExplored++;
		if (((int)fabs(currentDist) * 100) / 100 == 1.0)    //perfect correlation found, stop
		{
			cout << groupArray[groupID].centroidID << "[" << groupArray[groupID].centroidStart << "," << groupArray[groupID].centroidStart + length - 1 << "] " << "corr " << currentDist << endl;
			return currentDist;     //found the centroid with corr=1 , return
		}
		if (fabs(currentDist)>fabs(bsf))
		{
			bsf = currentDist;
			bsfIndex = groupID;
		}
		CentroidCorrVec[i] = fabs(currentDist);
	}


	std::vector<double>::iterator pos;
	int ArrayPos;

#if	(CORAL_ONLINE_S1_CHECKNCLUSTERONLY==1)
				 // ---------- NOTE: Code in this section was supposed to iterate over only top k groups. -----
	
				 // Find the top groups
	int nClusters2Visit = std::min(CORAL_ONLINE_S1_CLUSTERS2VISIT, (int)CentroidCorrVec.size());
	std::vector<int> Clusters2VisitIdVec(nClusters2Visit,0);


	for (int i = 0; i < nClusters2Visit; i++)
	{
		pos = std::max_element(CentroidCorrVec.begin(), CentroidCorrVec.end());
		ArrayPos = std::distance(CentroidCorrVec.begin(), pos);
		CentroidCorrVec[ArrayPos] = -100;
		Clusters2VisitIdVec[i] = Time[length - 1].groupid[ArrayPos];
	}

	CORALnCandidateClusters = nClusters2Visit;		// writes in output file number of candidates clusters selected for probing
#endif

#if	(CORAL_ONLINE_S1_CHECKNCLUSTERONLY==0)
	// Decide which clusters need to be visited as per criteria
	double Threshold = 1 - (ST * ST/ 2);
	int nClusters2Visit = 0;
	std::vector<int> Clusters2VisitIdVec;
	Clusters2VisitIdVec.reserve(numGroups);
	
	for (int i=0; i<numGroups;i++)
	{
		if (CentroidCorrVec[i] >= Threshold)
		{
			Clusters2VisitIdVec.push_back(Time[length - 1].groupid[i]);
		}

	}
	nClusters2Visit = Clusters2VisitIdVec.size();
	CORALnCandidateClusters = nClusters2Visit;		// writes in output file number of candidates clusters who pass threshold

	// In rare case no cluster may qualify. So push the best cluster so far
	if (nClusters2Visit == 0)
	{
		Clusters2VisitIdVec.push_back(bsfIndex);
		nClusters2Visit = 1;
	}
#endif

	//at this point have the best groups
	//compute with each sequence present in that group and get the best in each group
	vector<double> tempSeries;
	bsfTSIndex = 0;	
	int bestStart = 0;
	int bestEnd = 0;
	
	std::vector<double> bsfCorrVec(nClusters2Visit,0);
	std::vector<double> bsfAbsCorrVec(nClusters2Visit, 0);
	std::vector<int> bsfTSIndexVec(nClusters2Visit, 0);
	std::vector<int> bestStartVec(nClusters2Visit, 0);
	std::vector<int> bestEndVec(nClusters2Visit, 0);


	for (int i = 0; i < nClusters2Visit; i++)
	{
	dist = 0;
	bsf = 0;

	//currentDist = 0;
	bsfIndex = Clusters2VisitIdVec[i];

		for (int i = 0; i<groupArray[bsfIndex].seqObj.size(); i++)
		{
			int seqID = groupArray[bsfIndex].seqObj[i].id;
			int seqStart = groupArray[bsfIndex].seqObj[i].startT;
			int seqEnd = groupArray[bsfIndex].seqObj[i].endT;
			//copy this time series in tempSeries
			//z-normalize while copying the data
			for (int j = seqStart; j <= seqEnd; j++)
			{
				tempSeries.push_back((timeSeries[seqID][j] - groupArray[bsfIndex].seqObj[i].mean) / groupArray[bsfIndex].seqObj[i].std);
			}

			//compute point wise correlation distance
			dist = correlation(tempSeries, tempQ);
			seqExplored++;
			if (((int)fabs(dist) * 100) / 100 == 1.0)       //found perfect correlation ,stop
			{
				cout << seqID << "[" << seqStart << "," << seqEnd << "] " << "corr " << dist << endl;
				CORALBestTSId = seqID;
				CORALBestTSStart = seqStart;
				CORALBestTSEnd = seqEnd;
				CORALBestCorr = dist;
				return dist;

			}
			if (fabs(bsf)<fabs(dist))
			{
				bsf = dist;
				bsfTSIndex = seqID;
				bestStart = seqStart;
				bestEnd = seqEnd;
			}

			tempSeries.clear();
			dist = 0;


		}
		bsfCorrVec[i] = bsf;
		bsfAbsCorrVec[i] = fabs(bsf);
		bsfTSIndexVec[i] = bsfTSIndex;
		bestStartVec[i] = bestStart;
		bestEndVec[i] = bestEnd;

		//// if best absolute correlation within group doesn't imporve, simply exit the loop
		//if (i > 0 && bsfAbsCorrVec[i]<bsfAbsCorrVec[i-1])
		//{
		//	break;
		//}

	}

	// Find  the best of bests
	{
		pos = std::max_element(bsfAbsCorrVec.begin(), bsfAbsCorrVec.end());
		ArrayPos = std::distance(bsfAbsCorrVec.begin(), pos);
		bsf = bsfCorrVec[ArrayPos];
		bsfTSIndex = bsfTSIndexVec[ArrayPos];
		bestStart = bestStartVec[ArrayPos];
		bestEnd = bestEndVec[ArrayPos];

	}


	// cout << bsfTSIndex << "[" << bestStart << "," << bestEnd << "] " << " Corr " << bsf << endl;
	CORALBestTSId = bsfTSIndex;
	CORALBestTSStart = bestStart;
	CORALBestTSEnd = bestEnd;
	CORALBestCorr = bsf;
	return bsf;

#endif // CORAL_ONLINE_S1_CHECKMULTICLUSTER==1

}



void GroupOperation::corrSequences(int seq1, int start1, int end1, int seq2, int start2, int end2)
{
	vector<double> temp1;
	vector<double> temp2;
	//double value=0;
	for (int i = start1; i <= end1; i++)
	{
		temp1.push_back(timeSeries[seq1][i]);
	}
	for (int i = start2; i <= end2; i++)
	{
		temp2.push_back(timeSeries[seq2][i]);
	}

	cout << "Correlation " << correlation(temp1, temp2) << endl;
	temp1.clear();
	temp2.clear();
}
//outputs the self correlating sequences of time series TS passed that have correlation above
//groupVector contains the group ids TS subsequences belong to
void GroupOperation::selfCorrelation(int TS, int length)
{

	int count = 0;    //count of sequences in each group, output just count
	vector<int> groupVector;
	groupVector = PrintGroupCitizenship(TS, length);
	for (int i = 0; i<groupVector.size(); i++)
	{
		//iterate over each group
		int groupID = groupVector[i];
		Group *nGroup = new Group();
		*nGroup = groupArray[groupID];
		for (int j = 0; j<nGroup->seqObj.size(); j++)
		{
			if (nGroup->seqObj[j].id == TS)
			{
				//    cout<<TS<<"["<<nGroup->seqObj[j].startT<<"]"<<" ";
				count++;
			}
		}
	//	cout << count <<" ";
		count = 0;


	}
}
void GroupOperation::selfCorrNaive(int TS, int length, double corrThreshold)
{
	//for this length compute pair wise correlation between every subsequence of TS
	vector<double> temp1;        //containing temp first subsequence
	vector<double> temp2;        //containing temp 2nd subsequence
	double corrReturned = 0;
	for (int i = 0; i < L - length; i++)
	{
		//Check each subsequence
		//pick first subsequence
		temp1.clear();
		for (int p = i; p <= i + length; p++)
		{
			//copy  the subsequence in temp
			temp1.push_back(timeSeries[TS][p]);
		}
		//compare with every subsequene of other time series
		for (int k = i + 1; k < L - length; k++)
		{
			//pick 2nd subsequence
			temp2.clear();
			for (int q = k; q <= k + length; q++)
			{
				//copy  the subsequence in temp
				temp2.push_back(timeSeries[TS][q]);
			}

			//compute the correlation
			corrReturned = correlation(temp1, temp2);
		//	cout << "corrThreshold " << corrThreshold << endl;

			if (fabs(corrReturned) >= corrThreshold)
			{
			//	cout << corrReturned << " ";
			cout << corrReturned << " ";
			cout << endl << "Pair " << TS << "[" << i << "," << i + length << "] " << TS << "[" << k << "," << k + length << "] " << endl;
			}


		}
	}
}
//checks each group for length and prints group id if any subsequence of TS is present in it
void GroupOperation::printTSGroups(int TS, int length)
{
	int countGroups = Time[length].groupid.size();
	//iterate over each group
	for (int i = 0; i < countGroups; i++)
	{
		int groupID = Time[length].groupid[i];
		for (int j = 0; j < groupArray[groupID].count; j++)
		{
			if (groupArray[groupID].seqObj[j].id == TS)
			{
				cout << "G[" << groupID << "] ";
			}
		}

	}

}
//this function returns the sequences of the pair that have correlation above threshold and have longest length
//first 2 arguments are the time series id, minLength is the min length of sequences returned
void GroupOperation::longestCorrNaive(int TS1, int TS2, int minLength, double corrThreshold)
{
	vector<double> temp1;        //containing temp first subsequence
	vector<double> temp2;        //containing temp 2nd subsequence
	bool corrFound = false;       //if found 1 pair for longest length it is set to true, no need to explore for lesser lengths
								  //for each length starting from biggest find the seq
								  //return the pair that has correlation above threshold
	for (int z = L - 1; z >= minLength - 1; z--)
	{
		if (corrFound == true)
			break;
		int length = z;
		for (int i = 0; i<L - length; i++)
		{
			//Check each subsequence
			//pick first subsequence
			temp1.clear();
			//cout << "Current length " <<length <<endl;
			for (int p = i; p <= i + length; p++)
			{
				//copy  the subsequence in temp
				temp1.push_back(timeSeries[TS1][p]);
			}
			//compare with every subsequene of other time series
			for (int k = 0; k<L - length; k++)
			{
				//pick 2nd subsequence
				temp2.clear();
				for (int p = k; p <= k + length; p++)
				{
					//copy  the subsequence in temp
					temp2.push_back(timeSeries[TS2][p]);
				}

				//compute the correlation
				double corrReturned = correlation(temp1, temp2);
				if (fabs(corrReturned) >= corrThreshold)
				{
					corrFound = true;
					cout << "Pair " << "TS " << TS1 << "[" << i << "," << i + z << "] " << "TS " << TS2 << "[" << k << "," << k + z << "] " << " Corr " << corrReturned << endl;
					cout << "Longest Length found at" << length;
				}


			}

		}

	}
	if (corrFound == false)
		cout << "No Corr Longest found for all length > min Length";
}
//this function finds the longest length subsequence for the two time series
void GroupOperation::longestCORAL(int TS1, int TS2, int minLength)		//CORAL Operation of longest length subsequence
{
	vector<int> commonGroups;
	int retLength = 0;
	GetLongestMatchingSsGroups(TS1, TS2, minLength, commonGroups, retLength);
	if (retLength == 0)
		cout << "No Longest found " << endl;
	else
		cout << "Length " << retLength;

}
//CORAL function
void GroupOperation::getGroupsSeq(int length, double threshold, bool positiveOnly)
{
	int countGroups = 0;
	double corrValue = 0;	//correlation value 
	double tempValue = 0.82;	//for 0.3 ST value , for ST=0.4 use 0.68
	//double seqCorr = 1.0 - ((double)2.0 * );

	if (positiveOnly)		//true then only retrieve the groups
	{
		countGroups = Time[length].groupid.size();
		for (int i = 0; i < countGroups; i++)
		{
			//just print the sequence count in each group
			int tempGroupID = Time[length].groupid[i];
			cout << " [G " << tempGroupID << " " << groupArray[tempGroupID].count << "]";
		}
	}
	else
	{
		//this is extendible part
		//for each pair of rep check if their correlation is above the threshold
		int totalReps= Time[length].groupid.size();
		int seqCount = 0;
		vector<int> tempReps;	//temporary rep for the Rep under consideration
		for (int i = 0; i < totalReps; i++)
		{
			int tempGroupID1 = Time[length].groupid[i];
			seqCount = groupArray[tempGroupID1].count;
			for (int j = i + 1; j < totalReps; j++)
			{
				if (tempReps.size() == 0)	//there is no element
				{
					int tempGroupID2 = Time[length].groupid[j];
					corrValue = Time[length].InterRepCorrMap[MakeKey(tempGroupID1, tempGroupID2)];	//retreive the corr value between the two reps
					if (fabs(corrValue) >= threshold && (tempValue) >= threshold)
					{
						//this Ri and Rj can be combined
						seqCount = seqCount + groupArray[tempGroupID2].count;
						//adding this R2 to the vector of R1
						tempReps.push_back(tempGroupID2);
					}
				}
				else
				{//for each element in temp array check the value of corr between Reps
					//first check its corr with Ri, if it qualifies then check with others
					int tempGroupIDj = Time[length].groupid[j];
					corrValue = Time[length].InterRepCorrMap[MakeKey(tempGroupID1, tempGroupIDj)];	//retreive the corr value between the two reps
					if (fabs(corrValue) >= threshold && (tempValue) >= threshold)
					{
						int sizeV = tempReps.size();
						for (int k = 0; k < sizeV; k++)
						{
							int tempGroupIDi = tempReps[k];

							corrValue = Time[length].InterRepCorrMap[MakeKey(tempGroupIDi, tempGroupIDj)];	//retreive the corr value between the two reps
							if (fabs(corrValue) >= threshold)
							{
								seqCount = seqCount + groupArray[tempGroupIDj].count;
								tempReps.push_back(tempGroupIDj);


							}

						}
					}
				}
			}
			//COMMENT ANY PRINTING
		//	cout << " [G "<< tempGroupID1<<" " << seqCount << "]";
			tempReps.clear();

		}
	}
	//cout << endl;
}
//positive only true means only return the grous having positive correlation above threshold
//finds groups of sequences for a given length that have pair wise correlation above the threshold
void GroupOperation::naiveGroupCorr(double threshold, int length, bool positiveOnly)
{
	vector<double> temp1;        //containing temp first subsequence
	vector<double> temp2;        //containing temp 2nd subsequence
	osubsequenceGroup *tempSeq = new osubsequenceGroup;
	int countGroups = 0;
	//get each subsequence
	for (int i = 0; i<L - length; i++)
	{
		//Check each subsequence
		for (int j = 0; j<N; j++)
		{
			//pick first subsequence
			for (int p = i; p <= i + length; p++)
			{
				//copy  the subsequence in temp
				temp1.push_back(timeSeries[j][p]);
			}
			//place this first sequence in group
			osubsequenceGroup *tempSeq = new osubsequenceGroup;
			//add in the group
			tempSeq->id = j;
			tempSeq->startT = i;
			tempSeq->endT = tempSeq->startT + length;
			tempSeq->flag = true;
			tempSeq->anchor = true;
			groupSeq.push_back(*tempSeq);
			delete tempSeq;
			//need another iteration
			for (int m = i; m<L - length; m++)
			{
				for (int n = 0; n<N; n++)
				{
					//check if its same first sequence skip it
					if (m == i && n == j)
						;
					else if (n<j && m == i)
						;
					else
					{
						//pick 2nd subsequence
						for (int k = m; k <= m + length; k++)
							temp2.push_back(timeSeries[n][k]);

						double corrReturned = correlation(temp1, temp2);
						double corrValue = 0;
						if (positiveOnly)	//true means use that value as is, other wise take aboslute value of it
							corrValue = corrReturned;
						else
							corrValue = fabs(corrReturned);
						if (corrValue >= threshold)
						{
							osubsequenceGroup *tempSeq = new osubsequenceGroup;
							//add in the group
							tempSeq->id = n;
							tempSeq->startT = m;
							tempSeq->endT = tempSeq->startT + length;
							tempSeq->flag = true;    //temp setting each flag true
							groupSeq.push_back(*tempSeq);
							delete tempSeq;

						}
						temp2.clear();
					}
				}

			}
			temp1.clear();
			int countSeq = groupSeq.size();    //count of seq in group
											   //at this point temp group contains all the sequences that have correlation above threshold with first sequence
											   //now need to compare each pair in this
											   //start from 2nd sequence
			for (int i = 1; i<groupSeq.size() - 1; i++)
			{
				//only pick sequences with flag true
				if (groupSeq[i].flag == true)
				{
					osubsequenceGroup *tempSeq1 = new osubsequenceGroup;
					tempSeq1->id = groupSeq[i].id;
					tempSeq1->startT = groupSeq[i].startT;
					tempSeq1->endT = groupSeq[i].endT;
					for (int p = 0; p <= length; p++)
					{
						//copy  the subsequence in temp
						temp1.push_back(timeSeries[tempSeq1->id][tempSeq1->startT + p]);
					}
					for (int j = i + 1; j<groupSeq.size(); j++)
					{
						//pick the 2nd sequences if its flag is true
						if (groupSeq[j].flag == true)
						{

							osubsequenceGroup *tempSeq2 = new osubsequenceGroup;
							tempSeq2->id = groupSeq[j].id;
							tempSeq2->startT = groupSeq[j].startT;
							tempSeq2->endT = groupSeq[j].endT;
							for (int p = 0; p <= length; p++)
							{
								//copy  the subsequence in temp
								temp2.push_back(timeSeries[tempSeq2->id][tempSeq2->startT + p]);
							}
							//check if this pair has correlation above threshold
							double corrReturned = correlation(temp1, temp2);

							if (fabs(corrReturned)< threshold)
							{
								//remove the second sequence from the group
								//   groupSeq.erase(groupSeq.begin()+j);
								groupSeq[j].flag = false;
								countSeq--;

							}
							else
								groupSeq[j].flag = true;
							temp2.clear();
							delete tempSeq2;
						}


					}
					delete tempSeq1;
					temp1.clear();
				}
			}
			//print groupSeq here

			
			//can print sequences here too
			/*
			for (int i = 0; i < groupSeq.size(); i++)
			{
				cout << groupSeq[i].id << " [" << groupSeq[i].startT << "," << groupSeq[i].endT << " , ";
			}
			cout << endl;
			*/
			//COMMENT THE PRINTING
		//	if(countSeq!=1)	//print only if have more than 1 seq in group
		//		cout << "sequences in group " << countGroups++ << " " << countSeq << endl;
			groupSeq.clear();




		}
	}
}
