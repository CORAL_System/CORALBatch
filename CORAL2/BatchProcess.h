# pragma once
#include <string>
#include <vector>
#include <fstream>

#define CORAL_INSIDEQUERY 1
#define CORAL_OUTSIDEQUERY 2
#define CORAL_MINQUERYFLAG 1		// minimum query flag
#define CORAL_MAXQUERYFLAG 2		// maximum query flag

extern class GroupOperation;

class BatchProcess
{
public:
	BatchProcess();
	BatchProcess(void *);
	~BatchProcess();

	void Run(void);

	bool SetInputCommFileAddr(char * _InputCommFileAddr);
	bool SetTimeSeriesN(int _TimeSeriesN);
	bool SetTimeSeriesL(int _TimeSeriesL);

	//ADDED by RAMOZA
	bool SetST(double groupST);	//
	bool SetNumberRuns(int queryRuns);
	void runOtherOperations(void);
	bool SetInputDataSet(char *inputfilename);
	bool SetOutputFile(char *outputfilename);
	bool SetChoice(int _Choice);	//
	bool SetDelta(double _Delta);


private:

	std::ofstream h_OutputFile;		// Output file handle

	std::string InputCommFileAddr;
	std::string InputDatasetAddr;
	std::string OutputFileAddr;
	std::string OutputDirAddr;		// inside queries are written in this dir
	int MinLenInsideQuery = 0;		// minimum length of the inside query. All inside queries picked are greater than this length
	int f_QueryType = 0;			// query type flag. 1: inside queries. 2: outside queries
	int NumQueryProcess = 0;		// number of queries to be processed
	int CurrentQueryIdx = 0;		// iterator variable that iterates through all the queries to be processed
	void *m_groupObj;
	double ST = 0;
	int Choice = 0;
	double Delta = 0;

	// Outside query file name params
	std::string OutQueryFileAddrTmp;
	int OutQueryFileStartIdx = 0;
	int OutQueryFileEndIdx = 0;
	std::string OutQueryFileExt;	// extension is without dot


	std::vector <int> QueryTSIdVec;
	std::vector <int> QueryLenVec;
	std::vector <int> QueryOffsetVec;

	bool doCORALProcess = false;
	bool doNAIVEProcess = false;

	double CORALElaspedTime = 0;
	double NaiveElaspedTime = 0;
	double CORALError = 0;
	double TotalCORALElaspedTime = 0;
	double TotalNaiveElaspedTime = 0;
	double TotalCORALError = 0;

	std::string DL = ",";			// delimiter
	int TimeSeriesN = 0;			// no. of time series in dataset
	int TimeSeriesL = 0;			// length of each time series in dataset 
	// ------------------------- Private Methods -----------------------------------// 

	void ReadParams();
	void BadParamCheck();
	void RaiseError(bool DecisionFlag, std::string ErrStr);
	void RunCORAL();
	void RunNaive();
	void GenerateRandomQueries();
	int rand2(int a, int b);
	void LoadQuery();
	void WriteOutput();
	void Initialize();
	void DoWrapup();
	void Finalize();


	//ADDED BY RAMOZA
	double randCorr();		//added by Ramoza, to generate corr between 0-1
	void RunNaiveCorrGroups(int length, double corrThreshold);	//added by Ramoza
	void RunCORALCorrGroups(int length, double corrThreshold);	//added by Ramoza
	void RunCORALSelfCorr(int TS, int length);	//added by Ramoza
	void RunNaiveSelfCorr(int TS, int length);	//added by Ramoza
	void RunCORALLongestCorr(int TS1, int TS2, int length);	//added by Ramoza);
	void RunNaiveLongestCorr(int TS1, int TS2, int length);	//added by Ramoza);

};








