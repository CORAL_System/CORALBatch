#ifndef STRUCTURE_H
#define STRUCTURE_H
#endif // STRUCTURE_H
#include <vector>
#include <iostream>
#include <stdio.h>
#include<fstream>
#include <math.h>
#include <unordered_map>
using namespace std;

/*
struct intraDistance
{
    double distance;
    double flag;      //flag true means +ve, false means -ve
};
*/
//this structure will be used by Naivee method to retrieve group of correlated sequences
struct osubsequenceGroup
{
	int id;     //time series id
	int startT; //start interval of this subsequence
	int endT;   //end interval of this subsequence
	bool flag;      //this will be tue if part of final group, otherwise false
	bool anchor;    //this is true if seq has been anchored

};
struct subsequence
{
    int id;     //time series id
    int startT; //start interval of this subsequence
    int endT;   //end interval of this subsequence
    double mean;        //stored mean for later z-normalize the sequence
    double std;         //stored std for the sequence

};

struct Group
{

    int count;  //count of time series in this group
    int id;     //
    vector<double> centroid;    //centroid element array
    int centroidID; //this is time series id
    int centroidStart;  //start of this centroid sequence
    double mean;        //stored mean for later z-normalize the sequence
    double std;         //stored std for the sequence
    vector<subsequence> seqObj;


};

struct timeEntry
{
    vector<int> groupid;       //an array containing the id of each group in this time
	std::unordered_map < __int64, double > InterRepCorrMap;

};


