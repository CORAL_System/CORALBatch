# Introduction
This repository contains the research code for the CORAL(CORrelation based AnaLytics) algorithm. 

# How to run 
Double click on **DevelopmentRig.sln** which is the visual studio solution file. The solution has been developed with VS2015. CORAL project (inside VS solution) contains all the research code. However, the executable has been designed such that it runs one query at a time in interactive manner. On the other hand, CORAL2 project (inside VS solution) was designed to process queries in batch. 

# Related repositories
In order to benchmark CORAL algorithm against other algorithms, other projects were setup. Their repositories are as following:
- [PAA](https://gitlab.com/CORAL_System/PAA)
- [JACOR](https://gitlab.com/CORAL_System/JACOR)

**NOTE:** *JOCOR* algorithm has been represented by the string *JACOR*

Apart from that there are other miscellaneous repositories serving different purposes

| repo | description |
| ---- | ----------- |
| [MATLABWorkspace](https://gitlab.com/CORAL_System/MATLABWorkspace) | contains code to aggregate query results and generate plotting data |
| [CORAL-Additional](https://github.com/muzammil360/coral-additional2) | contains UCR archive dataset info and queries used for benchmarking and evaluation |


# How to get help
Feel free to open up an issue on gitlab. This is the preffered way to communicate issues or report bugs. 

# Contributors
- Muzammil Bashir (mbashir@wpi.edu)
- Ramoza Ahsan (ramoza@gmail.com)